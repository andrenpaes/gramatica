
class Abertura
  attr_dsl_accessor :tipo, :area, :comprimento, :distancia_origem, :altura, :peitoril
  # Define a parede 'logica', ou seja, dentro de uma
  # parede maior, a localizacao especifica da abertura.
  attr_dsl_accessor :segmento_aresta

  def initialize(options = {})
    area 0.0
    comprimento 0.0
    distancia_origem 0.0
    options.each do |field, value|
      value = Math.round(value) if value.is_a? Numeric
      send("#{field}", value)
    end
  end
end

class Coberta
  attr_accessor :altura, :espessura, :angulo_inclinacao
  attr_accessor :pontos, :baseline, :sinal_pivo

  def initialize
    @espessura = ParametrosConstrucao.espessura_coberta
    @angulo_inclinacao = Math.atan(ParametrosConstrucao.inclinacao_coberta)
    @pontos = []
  end
end

class Parede
  attr_accessor :aresta, :aberturas, :altura, :espessura

  def initialize(aresta)
    @altura = ParametrosConstrucao.altura_parede
    @espessura = ParametrosConstrucao.espessura_parede
    @aresta = aresta
    @aberturas = []
  end

  def sobreposta?(parede)
    @aresta.sobreposta?(parede.aresta)
  end

  def ligar!(parede)
    @aresta.ligar!(parede.aresta)
    @aberturas.push(*parede.aberturas)
  end

  def format
    "(#{aresta.inicio.x},#{aresta.inicio.y})=>(#{aresta.fim.x},#{aresta.fim.y})"
  end
end

class Zona
  attr_accessor :simbolo, :categoria, :paredes
  attr_reader :arestas, :pontos

  def initialize(pontos)
    @pontos = pontos.dup
    @arestas = []
    @paredes = []

    definir_arestas
    ordenar_coordenadas_arestas
  end

  def definir_arestas
    reta = Reta.new
    @pontos.each do |ponto|
      if reta.inicio
        reta.fim = ponto
        @arestas << reta

        reta = Reta.new ponto
      else
        reta.inicio = ponto
      end
    end

    reta.fim = @pontos.first
    @arestas << reta
  end

  def ordenar_coordenadas_arestas
    @arestas.each do |reta|
      reta.ordenar_coordenadas
    end
  end

  # So funciona para zonas retangulares
  def area
    dimensoes = @arestas.map { |_| _.comprimento }
    largura = dimensoes.first
    comprimento = dimensoes.find(largura) {|_| _ != largura }

    (largura - 0.15) * (comprimento - 0.15)
  end

  def aresta_adjacente(aresta)
    @arestas.find do |arestas_adjacentes|
      if aresta.comprimento >= arestas_adjacentes.comprimento
        aresta.sobreposta_exclusivo?(arestas_adjacentes)
      else
        arestas_adjacentes.sobreposta_exclusivo?(aresta)
      end
    end
  end

  # Dada uma aresta adjacente (ver aresta_adjacente) a zona atual,
  # calcula reta que representa o quanto as arestas estão sobreposas.
  # No exemplo abaixo, seria a reta apontada como intersecao.
  #
  #  ______________________________
  # |                      |       |
  # |        intersecao -> | zona  |
  # |                      |_______|
  # |fora da intersecao -> |       |
  # |______________________|_______|
  #                        ^
  #                      aresta
  #
  def intersecao_adjacencia(aresta)
    if (aresta_adjacente = self.aresta_adjacente(aresta))
      aresta.reta_de_overlap(aresta_adjacente)
    end
  end
end

class Planta
  attr_reader :zonas

  def initialize(zonas = [])
    @zonas = zonas
  end

  def zonas_adjacentes(zona)
    adjacentes = []
    zona.arestas.each do |aresta|
      adjacentes += zonas_adjacentes_aresta(zona, aresta)
    end

    adjacentes
  end

  # Procura zonas adjacentes pela aresta
  def zonas_adjacentes_aresta(zona, aresta)
    adjacentes = []
    @zonas.each do |outra_zona|
      if zona != outra_zona
        adjacentes << outra_zona if outra_zona.aresta_adjacente(aresta)
      end
    end

    adjacentes
  end
end

class Engine
  attr_accessor :gerador_ambientes

  def initialize
    @planta = Planta.new
  end

  def adicionar_zona(zona)
    @planta.zonas << zona
  end

  def gerar_paredes
    gerador_ambientes.gerar_ambientes(@planta)
    paredes = @planta.zonas.map { |zona| zona.paredes }.flatten

    ligar_paredes_sobrepostas(paredes)
  end

  def ligar_paredes_sobrepostas(paredes)
    paredes_sobrepostas = agrupar_paredes_sobrepostas(paredes)
    return paredes if paredes_sobrepostas.empty?

    paredes_ligadas = []
    paredes_sobrepostas.each do |parede_chave, sobrepostas|
      paredes_ligadas << ligar_paredes(parede_chave, *sobrepostas)
    end

    ligar_paredes_sobrepostas paredes_ligadas
  end

  def agrupar_paredes_sobrepostas(paredes)
    paredes_alinhadas = {}

    paredes.each do |parede|
      parede_chave = paredes_alinhadas.keys.find { |parede_chave| parede.sobreposta?(parede_chave) || parede_chave.sobreposta?(parede) }
      if parede_chave
        paredes_alinhadas[parede_chave] << parede
      else
        paredes_alinhadas[parede] = []
      end
    end

    return {} if paredes_alinhadas.all? {|_, sobrepostas| sobrepostas.empty? }
    paredes_alinhadas
  end

  def ligar_paredes(*paredes)
    resultante = paredes.first
    paredes.each do |parede|
      next if parede == resultante

      resultante.ligar!(parede)
    end

    resultante
  end

  def gerar_cobertas
    zonas_cobertas = @planta.zonas.select { |zona| zona.categoria != 'area_externa' && zona.categoria != 'muro_externo' }
    return [] if zonas_cobertas.empty?

    pontos = zonas_cobertas.map { |zona| zona.pontos }.flatten.sort
    cumeeira = calcular_cumeeira(pontos)
    [calcular_coberta_frente(pontos, cumeeira), calcular_coberta_fundo(pontos, cumeeira)]
  end

  def calcular_cumeeira(pontos)
    inicio_comprimento = pontos.first
    fim_comprimento = pontos.select { |ponto| ponto.x == inicio_comprimento.x && ponto != inicio_comprimento }.last
    comprimento_casa = inicio_comprimento.distancia_ponto(fim_comprimento)
    ponto_mais_direita = pontos.last

    Reta.new(Ponto.new(inicio_comprimento.x, inicio_comprimento.y + comprimento_casa / 2.0), Ponto.new(ponto_mais_direita.x, inicio_comprimento.y + comprimento_casa / 2.0))
  end

  def calcular_coberta_frente(pontos, cumeeira)
    coberta_frente = Coberta.new
    coberta_frente.altura = ParametrosConstrucao.altura_total - (ParametrosConstrucao.beiral_coberta_frente * ParametrosConstrucao.inclinacao_coberta)

    inicio_coberta = pontos.first
    inicio_coberta_beiral = inicio_coberta.translate_y(-ParametrosConstrucao.beiral_coberta_frente)
    coberta_frente.pontos << inicio_coberta_beiral
    coberta_frente.pontos << cumeeira.inicio
    coberta_frente.pontos << cumeeira.fim

    ponto_mais_direita_frente = pontos.select { |ponto| ponto.y == inicio_coberta.y }.last
    ponto_alinhado_fim_cumeeira_mais_direita = pontos.select { |ponto| ponto.x == cumeeira.fim.x }.first
    if ponto_mais_direita_frente != ponto_alinhado_fim_cumeeira_mais_direita
      coberta_frente.pontos << ponto_alinhado_fim_cumeeira_mais_direita.translate_y(-ParametrosConstrucao.beiral_coberta_fundo)

      ponto_dente_recuo_lateral = pontos.select {|ponto| ponto.x == ponto_mais_direita_frente.x && ponto.y ==  ponto_alinhado_fim_cumeeira_mais_direita.y }.first
      coberta_frente.pontos << ponto_dente_recuo_lateral.translate_y(-ParametrosConstrucao.beiral_coberta_fundo)
    end

    ponto_mais_direita_frente_beiral = ponto_mais_direita_frente.translate_y(-ParametrosConstrucao.beiral_coberta_frente)
    coberta_frente.pontos << ponto_mais_direita_frente_beiral
    coberta_frente.pontos << inicio_coberta_beiral # fecha poligono

    coberta_frente.baseline = Reta.new(inicio_coberta_beiral, ponto_mais_direita_frente_beiral)
    coberta_frente.sinal_pivo = 1

    coberta_frente
  end

  def calcular_coberta_fundo(pontos, cumeeira)
    coberta_fundo = Coberta.new
    coberta_fundo.altura = ParametrosConstrucao.altura_total - (ParametrosConstrucao.beiral_coberta_fundo * ParametrosConstrucao.inclinacao_coberta)

    inicio_coberta = pontos.select { |ponto| ponto.x == cumeeira.inicio.x }.last # ponto mais ao fundo
    inicio_coberta_beiral = inicio_coberta.translate_y(ParametrosConstrucao.beiral_coberta_fundo)
    coberta_fundo.pontos << inicio_coberta_beiral
    coberta_fundo.pontos << cumeeira.inicio
    coberta_fundo.pontos << cumeeira.fim

    ponto_mais_direita_fundo = pontos.select { |ponto| ponto.y == inicio_coberta.y }.last
    ponto_alinhado_fim_cumeeira_mais_direita = pontos.select { |ponto| ponto.x == cumeeira.fim.x }.last
    if ponto_mais_direita_fundo != ponto_alinhado_fim_cumeeira_mais_direita
      coberta_fundo.pontos << ponto_alinhado_fim_cumeeira_mais_direita.translate_y(ParametrosConstrucao.beiral_coberta_fundo)

      ponto_dente_recuo_lateral = pontos.select {|ponto| ponto.x == ponto_mais_direita_fundo.x && ponto.y ==  ponto_alinhado_fim_cumeeira_mais_direita.y }.first
      coberta_fundo.pontos << ponto_dente_recuo_lateral.translate_y(ParametrosConstrucao.beiral_coberta_fundo)
    end

    ponto_mais_direita_fundo_beiral = ponto_mais_direita_fundo.translate_y(ParametrosConstrucao.beiral_coberta_fundo)
    coberta_fundo.pontos << ponto_mais_direita_fundo_beiral
    coberta_fundo.pontos << inicio_coberta_beiral # fecha poligono

    coberta_fundo.baseline = Reta.new(inicio_coberta_beiral, ponto_mais_direita_fundo_beiral)
    coberta_fundo.sinal_pivo = 0

    coberta_fundo
  end
end

class State
  class << self
    def start_engine
      @engine = Engine.new
      @pontos = []
    end

    def criar_ponto(x, y)
      @pontos << Ponto.new(x, y)

      nil
    end

    def criar_zona(simbolo, categoria)
      zona = Zona.new(@pontos)
      zona.simbolo = simbolo.to_variable_name
      zona.categoria = categoria.to_variable_name
      @engine.adicionar_zona(zona)

      nil
    ensure
      @pontos.clear
    end

    def gerador_ambientes=(gerador_ambientes)
      @engine.gerador_ambientes = gerador_ambientes
    end

    def gerar_paredes
      @engine.gerar_paredes
    end

    def gerar_cobertas
      @engine.gerar_cobertas
    end
  end
end

State.start_engine
