# Definicao da gramatica
gramatica do
  numero_iteracoes 9
  direcao_fachada = ParametrosLote.direcao_fachada

  regra do
    producao do
      elemento(simbolo: :lote_raw,x: ParametrosLote.testada, y: ParametrosLote.comprimento)
    end
  end

  regra do
    simbolo_predecessor :lote_raw
    producao do
      elemento(simbolo: "Muro Lateral", x: ParametrosConstrucao.espessura_parede / 2.0, y: lote_raw.y, categoria: :muro_externo)
      elemento(simbolo: :lote, x: lote_raw.x - ParametrosConstrucao.espessura_parede, y: lote_raw.y - ParametrosConstrucao.espessura_parede / 2.0)
      elemento(simbolo: "Muro Fundo", x: lote_raw.x - ParametrosConstrucao.espessura_parede, y: ParametrosConstrucao.espessura_parede / 2.0, categoria: :muro_externo)
      elemento(simbolo: "Muro Lateral", x: ParametrosConstrucao.espessura_parede / 2.0, y: lote_raw.y, categoria: :muro_externo)
    end
  end

  regra do
    simbolo_predecessor :lote
    condicao { lote.x == 4.85 }
    producao do
      elemento(simbolo: "Recuo Frente", x: lote.x, y: 3.0, categoria: :area_externa)
      elemento(simbolo: :area_util, x: lote.x, y: lote.y - 3.0)
    end
  end

  regra do
    simbolo_predecessor :area_util
    producao do
      elemento(simbolo: :sala, x: area_util.x, y: 3.35)
      elemento(simbolo: :corpo, x: area_util.x, y: area_util.y - 3.35)
    end
  end

  regra do
    simbolo_predecessor :sala
    producao do
      elemento(simbolo: "Estar", x: sala.x - 1.65, y: sala.y, categoria: :estar)
      elemento(simbolo: "Recuo Lateral", x: 1.65, y: sala.y, categoria: :area_externa)
      inverter if direcao_fachada == :norte || direcao_fachada == :leste
    end
  end

  regra do
    simbolo_predecessor :corpo
    producao do
      elemento(simbolo: :quartos, x: corpo.x, y: 5.3)
      elemento(simbolo: :fundo_corpo, x: corpo.x, y: corpo.y - 5.3)
    end
  end

  regra do
    simbolo_predecessor :quartos
    producao do
      elemento(simbolo: "Circulacao", x: 0.95, y: quartos.y, categoria: :circulacao)
      elemento(simbolo: "Dormitorio", x: quartos.x - 0.95, y: quartos.y / 2.0, categoria: :dormitorio)
      elemento(simbolo: "Dormitorio", x: quartos.x - 0.95, y: quartos.y / 2.0, categoria: :dormitorio)
      inverter if direcao_fachada == :norte || direcao_fachada == :leste
    end
  end

  regra do
    simbolo_predecessor :fundo_corpo
    producao do
      elemento(simbolo: :fundo_casa, x: fundo_corpo.x, y: 5.5)
      elemento(simbolo: "Recuo Fundo", x: fundo_corpo.x, y: fundo_corpo.y - 5.5, categoria: :area_externa)
    end
  end

  regra do
    simbolo_predecessor :fundo_casa
    producao do
      elemento(simbolo: :servico, x: fundo_casa.x - 1.65, y: fundo_casa.y, categoria: :cozinha)
      elemento(simbolo: "Recuo Lateral", x: 1.65, y: fundo_casa.y, categoria: :area_externa)
      inverter if direcao_fachada == :norte || direcao_fachada == :leste
    end
  end

  regra do
    simbolo_predecessor :servico
    producao do
      elemento(simbolo: "Cozinha", x: servico.x, y: 3.35, categoria: :cozinha)
      elemento(simbolo: :area_servico, x: servico.x, y: servico.y - 3.35, categoria: :area_externa)
    end
  end

  regra do
    simbolo_predecessor :area_servico
    producao do
      elemento(simbolo: "Lavanderia", x: area_servico.x - 1.35, y: area_servico.y, categoria: :lavanderia)
      elemento(simbolo: "Banheiro", x: 1.35, y: area_servico.y, categoria: :banheiro)
      inverter if direcao_fachada == :norte || direcao_fachada == :leste
    end
  end
end
