module DynamicAttributtes
  def method_missing(method_id, *args)
    nome_metodo = method_id.id2name
    # TODO: /^[a-zA-Z_]+[a-zA-Z_0-9]*$/ =~ nome_metodo Regex ainda nao funciona
    if args.any?
      metaclass.send(:attr_dsl_accessor, nome_metodo)
      send(nome_metodo, *args)
    else
      super
    end
  end
end
