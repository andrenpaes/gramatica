module Ambientes
  class AmbienteMatcher
    def initialize(*matching_options)
      if matching_options.length > 1
        @matching_option = matching_options.map do |simbolo| {simbolo: simbolo}; end
      else
        matching_option = matching_options.first
        if matching_option.instance_of? Symbol
          @matching_option = {simbolo: matching_option}
        elsif matching_option.is_a? Hash
          @matching_option = matching_option
        else
          raise "Erro! Esperado Symbol ou Hash: #{arg}"
        end
      end
    end

    def matches?(zona)
      if @matching_option.instance_of? Hash
        @matching_option.all? {|atributo, valor| zona.send(atributo) == valor.to_s }
      else
        @matching_option.any? { |match_hash| match_hash.all? {|atributo, valor| zona.send(atributo) == valor.to_s } }
      end
    end
  end

  class AberturasBuilder
    attr_reader :ambiente, :parede, :aberturas

    def initialize(ambiente_origem, segmento_aresta, zonas_adjacentes = [])
      @ambiente = ambiente_origem
      @segmento_aresta = segmento_aresta
      @zonas_adjacentes = zonas_adjacentes
      # atalho para referenciar a parede na dsl
      @parede = @segmento_aresta
      @aberturas = []
    end

    def porta(options = {})
      options = {
        comprimento: ParametrosConstrucao.largura_porta,
        distancia_origem: ParametrosConstrucao.distancia_porta,
        altura: ParametrosConstrucao.altura_porta
      }.merge(options)
      criar_abertura(:porta, options)
    end

    def janela(options = {})
      options = {
        distancia_origem: parede.comprimento / 2.0,
        altura: ParametrosConstrucao.altura_janela,
        peitoril: ParametrosConstrucao.peitoril_janela
      }.merge(options)
      criar_abertura(:janela, options)
    end

    def vao(options = {})
      options = {
        comprimento: ParametrosConstrucao.largura_porta,
        altura: ParametrosConstrucao.altura_porta,
        distancia_origem: ParametrosConstrucao.espessura_parede / 2.0
      }.merge(options)
      criar_abertura(:vao, options)
    end

    def criar_abertura(tipo, options)
      @aberturas << Abertura.new({tipo: tipo, segmento_aresta: @segmento_aresta}.merge(options))
    end

    def adjacente?(simbolo_adjacente)
      @zonas_adjacentes.any? {|z| AmbienteMatcher.new(simbolo: simbolo_adjacente).matches?(z) }
    end
  end

  # Representa as definicoes de abertura de uma parede
  class DefinicaoAbertura
    attr_accessor :definicao

    def initialize(ambiente_match)
      if ambiente_match.instance_of? Symbol
        @ambiente_matcher = AmbienteMatcher.new(simbolo: ambiente_match)
      elsif ambiente_match.instance_of? Hash
        @ambiente_matcher = AmbienteMatcher.new(ambiente_match)
      else
        raise "Tipo de match para ambiente invalido: #{ambiente_match}:#{ambiente_match.class}"
      end
    end

    def gerar_aberturas(zona, segmento, zonas_adjacentes)
      builder = AberturasBuilder.new(zona, segmento, zonas_adjacentes)
      builder.instance_eval(&definicao)

      builder.aberturas
    end

    def corresponde?(zona)
      @ambiente_matcher.matches? zona
    end
  end

  # Representa a definicao de um ambiente
  class DefinicaoAmbiente
    def initialize(matcher = nil)
      @ambiente_matcher = matcher
      @definicoes_aberturas = []
    end

    def sem_paredes
      @sem_paredes = true
    end

    def muro_externo
      @muro_externo = true
    end

    def abertura_para(simbolo_ambiente_destino, &block)
      definicao_abertura = DefinicaoAbertura.new(simbolo_ambiente_destino)
      definicao_abertura.definicao = Proc.new(&block) if block_given?
      @definicoes_aberturas << definicao_abertura
    end

    def corresponde?(zona)
      @ambiente_matcher.matches?(zona)
    end

    def gerar_ambiente(zona, planta)
      return if @sem_paredes
      
      if @muro_externo
        gerar_muro_externo(zona, planta)
      else
        gerar_paredes(zona, planta)
      end
    end

    private

    def gerar_muro_externo(zona, planta)
      zona.arestas.each do |aresta|
        zonas_adjacentes_aresta = planta.zonas_adjacentes_aresta(zona, aresta)
        zona.paredes << Parede.new(aresta) if zonas_adjacentes_aresta.any? { |adjacente| adjacente.categoria != 'muro_externo' }
      end
    end

    def gerar_paredes(zona, planta)
      todas_zonas_adjacentes = planta.zonas_adjacentes(zona)
      zona.arestas.each do |aresta|
        parede = Parede.new(aresta)

        zonas_adjacentes_aresta = planta.zonas_adjacentes_aresta(zona, aresta)
        zonas_adjacentes_aresta.each do |adjacente|
          if (definicao_abertura = definicao_abertura_para(adjacente))
            segmento_aresta = adjacente.intersecao_adjacencia(aresta)
            parede.aberturas = definicao_abertura.gerar_aberturas(zona, segmento_aresta, todas_zonas_adjacentes)
          end
        end

        zona.paredes << parede
      end
    end

    def definicao_abertura_para(zona_adjacente)
      @definicoes_aberturas.find { |definicao| definicao.corresponde?(zona_adjacente) }
    end
  end

  class GeradorAmbientes
    attr_reader :definicoes_ambientes

    def initialize
      @definicoes_ambientes = []
    end

    def gerar_ambientes(planta)
      planta.zonas.each do |zona|
        definicao_ambiente = definicao_para_zona(zona)

        if definicao_ambiente
          paredes_definicao(zona, planta, definicao_ambiente)
        else
          paredes_padrao(zona)
        end
      end
    end

    def paredes_padrao(zona)
      zona.paredes = zona.arestas.map { |aresta| Parede.new(aresta) }
    end

    def paredes_definicao(zona, planta, definicao_ambiente)
      definicao_ambiente.gerar_ambiente(zona, planta)
    end

    def definicao_para_zona(zona)
      definicoes_ambientes.find { |definicao| definicao.corresponde?(zona) }
    end
  end

  class GeradorBuilder
    attr_reader :gerador_ambientes
    def initialize
      @gerador_ambientes = GeradorAmbientes.new
    end

    def ambiente(*matching_options, &block)
      matcher = AmbienteMatcher.new(*matching_options)
      ambiente = DefinicaoAmbiente.new(matcher)

      ambiente.instance_eval(&block)
      gerador_ambientes.definicoes_ambientes << ambiente
    end
  end
end

def ambientes(&block)
  gerador_builder = Ambientes::GeradorBuilder.new
  gerador_builder.instance_eval(&block) if block_given?

  State.gerador_ambientes = gerador_builder.gerador_ambientes
end

ambientes do
  ambiente categoria: :area_externa do
    sem_paredes
  end
  ambiente categoria: :muro_externo do
    muro_externo
  end

  ambiente :dormitorio do
    abertura_para :circulacao do
      porta
    end

    abertura_para :recuo_lateral do
      janela area: ambiente.area * 0.17
    end
  end

  ambiente :estar do
    abertura_para :circulacao do
      vao
    end

    abertura_para :recuo_frente do
      porta comprimento: 1.2
      janela area: ((ambiente.area * 0.2) - 0.8) unless adjacente? :recuo_lateral
    end

    abertura_para :recuo_lateral do
      janela area: ((ambiente.area * 0.2) - 0.8)
    end
  end

  ambiente :cozinha do
    abertura_para :circulacao do
      vao
    end
    abertura_para :lavanderia do
      porta
    end

    abertura_para categoria: :area_externa do
      janela area: ((ambiente.area * 0.2) - 0.65)
    end
  end

  ambiente :lavanderia do
    sem_paredes
  end

  ambiente :banheiro do
    abertura_para :cozinha do
      porta
    end

    abertura_para :recuo_fundo do
      janela area: ambiente.area * 0.13, altura: ParametrosConstrucao.altura_janela_banheiro, peitoril: ParametrosConstrucao.peitoril_janela_banheiro
    end
    abertura_para :recuo_lateral do
      janela area: ambiente.area * 0.13, altura: ParametrosConstrucao.altura_janela_banheiro, peitoril: ParametrosConstrucao.peitoril_janela_banheiro unless adjacente? :recuo_fundo
    end
  end
end
