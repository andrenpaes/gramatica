module Categorias
  @categorias = {
      :area_externa => "Area Externa",
      :muro_externo => "Muro Externo",
      :dormitorio => "Dormitorio",
      :estar => "Estar",
      :circulacao => "Circulacao",
      :cozinha => "Cozinha",
      :lavanderia => "Lavanderia",
      :banheiro => "Banheiro"
  }

  def self.[](simbolo)
    @categorias[simbolo]
  end
end

class ParametrosLote
  class << self
    attr_accessor :testada, :comprimento, :azimute, :tipo_lote

    def direcao_fachada
      return nil unless azimute

      return :norte if azimute >= 45 && azimute < 135
      return :leste if azimute >= 135 && azimute < 225
      return :sul if azimute >= 225 && azimute < 315
      return :oeste
    end
  end
end

class ParametrosConstrucao
  class << self
    attr_accessor :espessura_parede, :altura_parede, :espessura_laje, :altura_total
    attr_accessor :largura_porta, :altura_porta, :distancia_porta
    attr_accessor :altura_janela, :altura_janela_banheiro, :peitoril_janela, :peitoril_janela_banheiro
    attr_accessor :espessura_coberta, :inclinacao_coberta, :beiral_coberta_frente, :beiral_coberta_fundo
  end
end

ParametrosConstrucao.espessura_parede = 0.15
ParametrosConstrucao.altura_parede = 2.5
ParametrosConstrucao.espessura_laje = 0.1
ParametrosConstrucao.altura_total = ParametrosConstrucao.altura_parede + ParametrosConstrucao.espessura_laje

ParametrosConstrucao.largura_porta = 0.8
ParametrosConstrucao.altura_porta = 2.1
ParametrosConstrucao.distancia_porta = 0.1 + (ParametrosConstrucao.espessura_parede / 2.0)

ParametrosConstrucao.altura_janela = 1.2
ParametrosConstrucao.altura_janela_banheiro = 0.4
ParametrosConstrucao.peitoril_janela = 0.9
ParametrosConstrucao.peitoril_janela_banheiro = 1.7

ParametrosConstrucao.espessura_coberta = 0.15
ParametrosConstrucao.inclinacao_coberta = 0.3 # 30%
ParametrosConstrucao.beiral_coberta_frente = 1.65
ParametrosConstrucao.beiral_coberta_fundo = 0.6
