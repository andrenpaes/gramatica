
module Gramatica

  class Elemento
    include DynamicAttributtes

    def initialize(options = {}, &block)
      options.each { |k, v| send(k, v) }
      instance_eval(&block) if block_given?
    end

    def categoria(*args)
      if args.any?
        @categoria = Categorias[args.first]
      end

      @categoria
    end
  end

  class Container
    include DynamicAttributtes
    
    def initialize(options = {})
      options.each { |k, v| send(k, v) }
    end
  end

  class Regra
    attr_dsl_accessor :simbolo_predecessor, :nome
    attr_reader :predecessor
    attr_writer :definicao_producao, :definicao_condicao

    # private
    def definir_predecessor(elemento_predecessor)
      @predecessor = elemento_predecessor
      metaclass.send(:alias_method, @simbolo_predecessor.to_sym, :predecessor) if @simbolo_predecessor
      yield
    ensure
      @predecessor = nil
    end

    def se_aplica(elemento_predecessor)
      definir_predecessor(elemento_predecessor) do
        if elemento_predecessor == nil
          return true if @simbolo_predecessor == nil
        elsif elemento_predecessor.simbolo.to_s == @simbolo_predecessor.to_s && condicao
          return true
        end

        false
      end
    end
    
    def condicao
      return self.instance_eval(&@definicao_condicao) if @definicao_condicao

      true
    end

    def construir_producao(elemento_predecessor)
      definir_predecessor(elemento_predecessor) do
        @elementos = []
        producao
        @elementos
      end
    ensure
      @elementos = nil
    end

    def producao
      self.instance_eval(&@definicao_producao) if @definicao_producao
    end

    def elemento(fields = {}, &block)
      @elementos << Elemento.new(fields, &block)
    end

    def inverter
      @elementos.reverse!
    end
  end

  class Gramatica
    attr_reader :regras, :resultado_iteracoes, :resultado
    attr_dsl_accessor :axioma, :numero_iteracoes

    def initialize
      @regras = []
    end

    def processar
      @resultado_iteracoes = []
      sequencia_atual = aplicar_regra(@axioma)
      @resultado_iteracoes << sequencia_atual

      (@numero_iteracoes - 1).times do
        sequencia_atual = processar_sequencia(sequencia_atual)
        @resultado_iteracoes << sequencia_atual
        @resultado = sequencia_atual
      end
      puts @resultado
    end

    def processar_sequencia(sequencia)
      resultado = []
      sequencia.each do |elemento|
        if elemento.respond_to? :sub_elementos
          produto = processar_sequencia(elemento.sub_elementos)
        else
          produto = aplicar_regra(elemento)
        end

        if produto.size == 1
          resultado << produto.first
        else
          container = Container.new(x: elemento.x, y: elemento.y, sub_elementos: produto)
          resultado << container
        end
      end

      resultado
    end

    def aplicar_regra(elemento)
      regras = regra_para_elemento(elemento)
      # seleciona uma regra aleatoriamente ou a unica que existir
      regra = regras[rand(regras.size)]

      if regra
        regra.construir_producao(elemento)
      else
        [elemento]
      end
    end

    def regra_para_elemento(elemento)
      @regras.select { |regra| regra if regra.se_aplica(elemento) }
    end
  end

  class GramaticaBuilder
    attr_accessor :gramatica

    def initialize
      @gramatica = Gramatica.new
    end

    def regra(&block)
      @regra_atual = Regra.new
      @gramatica.regras << @regra_atual

      self.instance_eval(&block)
    ensure
      @regra_atual = nil
    end

    def condicao(&block)
      raise NoMethodError unless @regra_atual
      @regra_atual.definicao_condicao = block
    end

    def producao(&block)
      raise NoMethodError unless @regra_atual
      @regra_atual.definicao_producao = block
    end

    def method_missing(method_id, *args)
      if @gramatica.respond_to?(method_id)
        @gramatica.send(method_id, *args)
      elsif @regra_atual.respond_to?(method_id)
        @regra_atual.send(method_id, *args)
      else
        super
      end
    end
  end
end

def gramatica(&block)
  engine = Gramatica::Engine.new

  ParametrosLote.testada = engine.testada
  ParametrosLote.comprimento = engine.comprimento
  ParametrosLote.azimute = engine.azimute
  ParametrosLote.tipo_lote = engine.tipo_lote

  builder = Gramatica::GramaticaBuilder.new
  builder.instance_eval(&block)

  engine.gramatica = builder.gramatica
  engine.run

  builder.gramatica
end
