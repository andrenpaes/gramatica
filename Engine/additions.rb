
class Class
  def attr_dsl_accessor(*args)
    args.each do |attr_name|
      define_method(attr_name) do |*value|
        instance_variable_set("@#{attr_name}", value.first) unless value.empty?
        instance_variable_get("@#{attr_name}")
      end
    end
  end
end

class Object
	def metaclass
    class << self; self end
  end
end

class String
  def to_variable_name
    variable_name = self.gsub ' ', '_'

    variable_name.downcase
  end
end

module Math
  def self.round(x, precision = 4)
    x.round(precision)
  end

  def self.abs(x)
    return x if x >= 0
    x * -1
  end

  def self.between(i1, i2, value)
    if i1 <= i2
      value >= i1 && value <= i2
    else i2 < i1
      value >= i2 && value <= i1
    end
  end

  def self.between_exclusive(i1, i2, value)
    return true if (i1 == i2) &&  i1 == value
    if i1 <= i2
      value > i1 && value < i2
    else i2 < i1
      value > i2 && value < i1
    end
  end
end
