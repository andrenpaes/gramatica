class Ponto
  include Comparable

  attr_reader :x, :y

  def initialize(x, y)
    @x = Math.round(x.to_f)
    @y = Math.round(y.to_f)
  end

  def distancia_ponto(ponto)
    Ponto.distancia(self, ponto)
  end

  def translate_x(offset)
    Ponto.new(x + offset, y)
  end

  def translate_y(offset)
    Ponto.new(x, y + offset)
  end

  def between?(p1, p2)
    Math.between(p1.x, p2.x, @x) && Math.between(p1.y, p2.y, @y)
  end

  def ==(other)
    return true if eql? other
    return x == other.x && y == other.y if other.is_a? Ponto

    false
  end

  def <=>(other)
    comparison = x <=> other.x
    comparison = y <=> other.y if comparison == 0

    comparison
  end

  def to_s
    "#<Ponto:#{__id__} @x=#{x}, @y=#{y}>"
  end

  def hash
    initial = 5
    multiplier = 79
    [x, y].inject(initial) do |hash_code, componente|
      hash_code * multiplier + componente.hash
    end
  end

  def Ponto.distancia(p1, p2)
    Math.round(Math.sqrt((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2))
  end
end

class Reta
  attr_accessor :inicio, :fim

  def initialize(inicio = nil, fim = nil)
    @inicio = inicio
    @fim = fim
  end

  # So funciona para retas verticais ou horizontais
  def ordenar_coordenadas
    if @inicio > @fim
      old_inicio = @inicio
      @inicio = @fim
      @fim = old_inicio
    end
  end

  #
  # Calcula overlap entre duas retas a e b, assumindo que ambas tem a mesma equacao
  #
  # ia---ib--fa--fb
  #       |--| <- overlap -- interescao
  #
  def reta_de_overlap(reta)
    if self.contem_reta?(reta)
      inicio_overlap = reta.inicio
      fim_overlap = reta.fim
    elsif reta.contem_reta?(self)
      inicio_overlap = self.inicio
      fim_overlap = self.fim
    else
      if reta.inicio.between?(@inicio, @fim)
        inicio_overlap = reta.inicio
      else
        inicio_overlap = reta.fim
      end
      if @inicio.between?(reta.inicio, reta.fim)
        fim_overlap = @inicio
      else
        fim_overlap = @fim
      end
    end

    intersecao_adjacencia = Reta.new(inicio_overlap, fim_overlap)
    intersecao_adjacencia.ordenar_coordenadas

    intersecao_adjacencia
  end

  def ligar!(reta)
    return if contem_reta?(reta) || !sobreposta?(reta)

    pontos = [self.inicio, self.fim, reta.inicio, reta.fim].sort
    self.inicio = pontos.first
    self.fim = pontos.last
  end

  def comprimento
    Ponto.distancia(@inicio, @fim)
  end

  def alinhada?(reta)
    colinear?(reta.inicio) && colinear?(reta.fim)
  end

  def sobreposta?(reta)
    if self.contem_ponto?(reta.inicio)
      return true if self.colinear?(reta.fim)
    elsif self.contem_ponto?(reta.fim)
      return true if self.colinear?(reta.inicio)
    elsif reta.contem_ponto?(self.inicio)
      return true if reta.colinear?(self.fim)
    elsif reta.contem_ponto?(self.fim)
      return true if reta.colinear?(self.inicio)
    end

    false
  end

  def sobreposta_exclusivo?(reta)
    if contem_ponto_exclusivo?(reta.inicio)
      return true if colinear?(reta.fim)
    elsif contem_ponto_exclusivo?(reta.fim)
      return true if colinear?(reta.inicio)
    elsif self == reta
      return true
    end

    false
  end

  def contem_reta?(reta)
    contem_ponto?(reta.inicio) && contem_ponto?(reta.fim)
  end

  def colinear?(ponto)
    crossproduct = Math.round((ponto.y - inicio.y) * (fim.x - inicio.x) -
                                  (ponto.x - inicio.x) * (fim.y - inicio.y))
    return false if Math.abs(crossproduct) != 0

    true
  end

  def contem_ponto?(ponto)
    return false unless colinear? ponto

    dotproduct = Math.round((ponto.x - inicio.x) * (fim.x - inicio.x) +
                                (ponto.y - inicio.y) * (fim.y - inicio.y))
    return false if dotproduct < 0

    return false unless Math.between(inicio.x, fim.x, ponto.x) && Math.between(inicio.y, fim.y, ponto.y)

    true
  end

  def contem_ponto_exclusivo?(ponto)
    return false unless colinear? ponto


    dotproduct = Math.round((ponto.x - inicio.x) * (fim.x - inicio.x) +
                                (ponto.y - inicio.y) * (fim.y - inicio.y))
    return false if dotproduct < 0
    return false unless Math.between_exclusive(inicio.x, fim.x, ponto.x) && Math.between_exclusive(inicio.y, fim.y, ponto.y)

    true
  end

  def ==(other)
    return true if eql? other
    if other.is_a? Reta
      return inicio == other.inicio && fim == other.fim ||
          inicio == other.fim && fim == other.inicio
    end

    false
  end

  def hash
    initial = -3
    multiplier = -17
    [inicio, fim].sort.inject(initial) do |hash_code, ponto|
      hash_code * multiplier + ponto.hash
    end
  end

  def to_s
    "#<Reta:#{__id__} @inicio=#{inicio}, @fim=#{fim}>"
  end
end
