/**
 * @file Contains the Macintosh-specific resources.
 */


#include "SysTypes.r"
#include "Types.r"
#include "Appearance.r"

#include "DGDefs.h"
#include "DGMacResType.r"

#include "ResTmpl.r"

#include "Gramatica.grc.ro"
