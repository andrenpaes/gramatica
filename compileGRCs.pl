#! /usr/bin/perl -w

use FindBin ;
use File::Spec::Functions;
use Cwd;

#-----------------------------------------------------------------------
# Executes one GRC conversion command
#-----------------------------------------------------------------------
sub DoGRC ($$$$$)
{
	my ($tool, $sourceDir, $inputFile, $destinationDir, $outputFile) = @_ ;

	system ("mkdir -p \"" . $destinationDir . "\"") ;
	print "\t$inputFile\n";
	system ($tool . " -m r -q utf8 0 -T M -i \"" . catfile ($sourceDir, $inputFile) . "\" -j .roo -p \"" . catfile ($sourceDir, "Images") . "\" -o \"" . catfile ($destinationDir, $outputFile) . "\"");
}


#-----------------------------------------------------------------------
# Convert GRC
#-----------------------------------------------------------------------
print "Converting resource files:\n";

#my $devKitPath = catfile ($FindBin::Bin, updir (), updir (), updir ());				# for file in the Make.mac folder
my $devKitPath = catfile ($FindBin::Bin, updir (), updir ());
my $makeResConvTool = "\"" . catfile (Cwd::realpath ($devKitPath), "Support", "Tools", "OSX", "ResConv") . "\"";

#my $examplePath = Cwd::realpath (catfile ($FindBin::Bin, updir ()));				# if it's in the Make.mac folder
my $examplePath = Cwd::realpath ($FindBin::Bin);

DoGRC ( $makeResConvTool, $examplePath, "Resources/Gramatica.grc", catfile ($examplePath, "RO"), "Gramatica.grc.ro" ) ;

print "done.\n"

