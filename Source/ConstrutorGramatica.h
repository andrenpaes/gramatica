//
//  ConstrutorGramatica.h
//  Gramatica
//
//  Created by Andre Rodrigues on 02/10/12.
//
//

#ifndef __Gramatica__ConstrutorGramatica__
#define __Gramatica__ConstrutorGramatica__

#include "GramaticaAttributes.h"
#include "ParametrosLote.h"

#include <vector>

struct IEngineGramatica;

struct Sequencia;
struct Elemento;
struct Container;


class ConstrutorGramatica {
private:
	IEngineGramatica* engine;
	const Sequencia* resultado;
	const GramaticaAttributes& attributes;

	void ConstruirSequencia(const Sequencia* sequencia, float xOrigin, float yOrigin) const;
	void ConstruirContainer(const Container* container, float xOrigin, float yOrigin) const;
	void ConstruirElemento(const Elemento* elemento, float xOrigin, float yOrigin) const;
public:
	ConstrutorGramatica();
	~ConstrutorGramatica();

	int Processar(const ParametrosLote *parametros);
	const char* GetDescricaoErro() const;
	void Construir(float xOrigin, float yOrigin) const;
};


#endif /* defined(__Gramatica__ConstrutorGramatica__) */
