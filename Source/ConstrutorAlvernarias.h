//
//  ConstrutorAlvernarias.h
//  Gramatica
//
//  Created by Andre Rodrigues on 27/05/13.
//
//

#ifndef __Gramatica__ConstrutorAlvernarias__
#define __Gramatica__ConstrutorAlvernarias__

#include <vector>

#include "GramaticaAttributes.h"

#include "ACAPinc.h"

struct IEngineAlvenarias;

class ConstrutorAlvernarias {
private:
	IEngineAlvenarias* engine;
	const GramaticaAttributes& attributes;
	std::vector<API_Element> zonas;

	void ConstruirParedes() const;
	void ConstruirLaje() const;
	void ConstruirCoberta() const;
public:
	ConstrutorAlvernarias();
	~ConstrutorAlvernarias();

	void AdicionarZona(API_Element elementoZona);

	int Processar();
	const char* GetDescricaoErro() const;
	void Construir() const;
};

#endif /* defined(__Gramatica__ConstrutorAlvernarias__) */
