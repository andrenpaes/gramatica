//
//  EngineRuby.h
//  Gramatica
//
//  Created by Andre Rodrigues on 28/11/13.
//
//

#ifndef __Gramatica__EngineRuby__
#define __Gramatica__EngineRuby__

#include "IEngine.h"
#include "mruby.h"

#define HANDLE_ERROR() (HandleError(__LINE__))

struct EngineRuby {
protected:
	mrb_state* mrb;
	
	EngineError ExecuteRubyFile(const char *file);
	EngineError HandleError(int lineNumber);
	
	virtual IEngine* GetIEngineInstance() = 0;
public:
	EngineRuby();
	~EngineRuby();
};

#endif /* defined(__Gramatica__EngineRuby__) */
