//
//  IEngineAlvenarias.h
//  Gramatica
//
//  Created by Andre Rodrigues on 02/06/13.
//
//

#ifndef __Gramatica__IEngineAlvenarias__
#define __Gramatica__IEngineAlvenarias__

#include <vector>

#include "IEngine.h"
#include "Attributes.h"
#include "Geometria.h"

class Zona {
private:
	std::vector<Coordenada> coordenadas;
	char* simbolo;
	char* categoria;
public:
	Zona(const char* simbolo, const char* categoria);
	~Zona();

	std::vector<Coordenada>& GetCoordenadas() {
		return coordenadas;
	};

	const char* GetSimbolo() const {
		return simbolo;
	}

	const char* GetCategoria() const {
		return categoria;
	}
};

class Abertura {
private:
	char* tipo;
	double area, peitoril;
	double comprimento;
	double altura, distanciaOrigem;

	Reta* retaReferencia;
public:
	Abertura(const char* tipo);
	~Abertura();

	const char* GetTipo() const {
		return tipo;
	}

	const Reta& GetRetaReferencia() const {
		return *retaReferencia;
	}
	void SetRetaReferencia(Reta *retaReferencia) {
		this->retaReferencia = retaReferencia;
	}

	double GetArea() const {
		return area;
	}
	void SetArea(double area) {
		this->area = area;
	}

	double GetPeitoril() const {
		return peitoril;
	}
	void SetPeitoril(double peitoril) {
		this->peitoril = peitoril;
	}

	double GetComprimento() const {
		return comprimento;
	}
	void SetComprimento(double comprimento) {
		
		this->comprimento = comprimento;
	}

	double GetAltura() const {
		return altura;
	}
	void SetAltura(double altura) {
		this->altura = altura;
	}

	double GetDistanciaOrigem() const {
		return distanciaOrigem;
	}
	void SetDistanciaOrigem(double distanciaOrigem) {
		
		this->distanciaOrigem = distanciaOrigem;
	}
};

class Parede {
private:
	double altura, espessura;
	const Reta reta;
	std::vector<Abertura*> aberturas;

public:
	Parede(double xi, double yi, double xf, double yf);
	~Parede();

	double GetAltura() const {
		return altura;
	}
	void SetAltura(double altura) {
		this->altura = altura;
	}

	double GetEspessura() const {
		return espessura;
	}
	void SetEspessura(double espessura) {
		this->espessura = espessura;
	}

	const Coordenada GetInicio() const {
		return reta.GetInicio();
	};

	const Coordenada GetFim() const {
		return reta.GetFim();
	};

	std::vector<Abertura*>& GetAberturas() {
		return aberturas;
	}
};

class Coberta {
private:
	double altura, espessura, anguloInclinacao;
	const Reta baseline;
	const int sinalPivo;
	std::vector<Coordenada> pontos;
public:
	Coberta(Reta baseline, int sinalPivo): baseline(baseline), sinalPivo(sinalPivo) {}

	double GetAltura() const {
		return altura;
	}
	void SetAltura(double altura) {
		this->altura = altura;
	}

	double GetEspessura() const {
		return espessura;
	}
	void SetEspessura(double espessura) {
		this->espessura = espessura;
	}

	double GetAnguloInclinacao() const {
		return anguloInclinacao;
	}
	void SetAnguloInclinacao(double anguloInclinacao) {
		this->anguloInclinacao = anguloInclinacao;
	}

	const Reta GetBaseline() const {
		return baseline;
	}

	const int GetSinalPivo() const {
		return sinalPivo;
	}

	std::vector<Coordenada>& GetPontos() {
		return pontos;
	}
};

struct IEngineAlvenarias : public IEngine {
protected:
	std::vector<Parede*> paredes;
	std::vector<Coberta*> cobertas;
public:
	virtual ~IEngineAlvenarias();
	
	virtual EngineError Process() = 0;
	virtual EngineError LoadInfoZona(Zona& zona) = 0;

	const std::vector<Parede*>& GetResultadoParedes() const {
		return paredes;
	};

	const std::vector<Coberta*>& GetResultadoCobertas() const {
		return cobertas;
	};
};

IEngineAlvenarias* CreateAlvenariasEngine();

#endif /* defined(__Gramatica__IEngineAlvenarias__) */
