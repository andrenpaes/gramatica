/*
 *  Geometria.h
 *  Alvenaria
 *
 *  Created by André Rodrigues on 11/2/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

extern double arredondar(double value, double precision = 0.0001);

class Coordenada {
private:
	double x, y;
public:
	Coordenada(): x(0), y(0) {};
	Coordenada(double x, double y): x(x), y(y) {};

	double GetX() const {return x;};
	void SetX(double x) { this->x = x;};

	double GetY() const {return y;};
	void SetY(double y) { this->y = y;};

	double distancia(const Coordenada& other) const;

	Coordenada operator=(const Coordenada& other);
	bool operator==(const Coordenada& other) const;
	bool operator!=(const Coordenada& other) const;
};

class Reta {
private:
	const Coordenada inicio;
	const Coordenada fim;
	
public:
	Reta(double xi, double yi, double xf, double yf);
	
	const Coordenada GetInicio() const {
		return inicio;
	};
	
	const Coordenada GetFim() const {
		return fim;
	};

	double Comprimento() const;

	bool operator==(const Reta& other) const;
	bool operator!=(const Reta& other) const;
};

class VetorNormalizado2d {
  public:
	VetorNormalizado2d(double xi, double yi, double xf, double yf);
	Coordenada GetCoordenada() const {return coordenada;};
	double GetAlpha() const {return alpha;};

	void rotacionar(double alpha);
	VetorNormalizado2d inverso() const;

	VetorNormalizado2d operator=(const VetorNormalizado2d& other);
	bool operator==(const VetorNormalizado2d& other) const;
	bool operator!=(const VetorNormalizado2d& other) const;

  private:
	Coordenada coordenada;
	double alpha;
};
