//
//  ConstrutorGramatica.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 02/10/12.
//
//

#include "ConstrutorGramatica.h"

#include "ACAPinc.h"

#include "IEngineGramatica.h"
#include "EngineLoader.h"

#include "Geometria.h"

ConstrutorGramatica::ConstrutorGramatica(): attributes(GramaticaAttributes::GetInstance()) {
	engine = CreateGramaticaEngine();
}
ConstrutorGramatica::~ConstrutorGramatica() {
	delete engine;
}

int ConstrutorGramatica::Processar(const ParametrosLote *parametros) {
	if (int erro = Inicializar(engine)) {
		return erro;
	}
	if (int erro = engine->Process(parametros)) {
		return erro;
	}
	resultado = engine->GetResultado();

	return 0;
}

const char* ConstrutorGramatica::GetDescricaoErro() const {
	return engine->GetErrorDescription();
}

void ConstrutorGramatica::Construir(float xOrigin, float yOrigin) const {
	ACAPI_OpenUndoableSession("Gerar!");
	ConstruirSequencia(resultado, xOrigin, yOrigin);
	ACAPI_CloseUndoableSession();
}

void ConstrutorGramatica::ConstruirSequencia(const Sequencia *sequencia,
											float xOrigin, float yOrigin) const {
	const std::vector<Node*> &elementos = sequencia->GetElementos();
	std::vector<Node*>::const_iterator it;
	for (it = elementos.begin(); it < elementos.end(); it++) {
		Node *node = *it;
		if (Container *container = dynamic_cast<Container*>(node)) {
			ConstruirContainer(container, xOrigin, yOrigin);
		} else if (Elemento *elemento = dynamic_cast<Elemento*>(node)) {
			ConstruirElemento(elemento, xOrigin, yOrigin);
		}

		yOrigin += node->GetY();
	}
}

void ConstrutorGramatica::ConstruirContainer(const Container *container,
											float xOrigin, float yOrigin) const {
	const std::vector<Node*> &elementos = container->GetSubElementos();

	float xStep = 0;
	float yStep = 0;
	std::vector<Node*>::const_iterator it;
	for (it = elementos.begin(); it < elementos.end(); it++) {
		Node *node = *it;
		if (Container *container = dynamic_cast<Container*>(node)) {
			ConstruirContainer(container, xOrigin + xStep, yOrigin + yStep);
		} else if (Elemento *elemento = dynamic_cast<Elemento*>(node)) {
			ConstruirElemento(elemento, xOrigin + xStep, yOrigin + yStep);
		}

		yStep += node->GetY();
		if (arredondar(yStep) >= arredondar(container->GetY())) {
			yStep = 0;
			xStep += node->GetX();
		}
	}
}

void ConstrutorGramatica::ConstruirElemento(const Elemento *elemento,
										   const float xOrigin, const float yOrigin) const {
	API_Element elementoZona;
	BNZeroMemory(&elementoZona, sizeof(API_Element));
	API_ElementMemo memoElementoZona;
	BNZeroMemory(&memoElementoZona, sizeof(API_ElementMemo));

	elementoZona.header.typeID = API_ZoneID;
	ACAPI_Element_GetDefaults(&elementoZona, &memoElementoZona);

	elementoZona.header.layer = attributes.IndexLayerZonas();
	elementoZona.zone.manual = true;
	elementoZona.zone.refPos.x = xOrigin;
	elementoZona.zone.refPos.y = yOrigin;

	elementoZona.zone.pos.x = xOrigin + (elemento->GetX() / 2);
	elementoZona.zone.pos.y = yOrigin + (elemento->GetY() / 2);

	strncpy(elementoZona.zone.roomName, elemento->GetSimbolo(), API_AttrNameLen);
	elementoZona.zone.catInd = attributes.IndexZona(elemento->GetCategoria());

	elementoZona.zone.poly.nCoords = 5;
	elementoZona.zone.poly.nSubPolys = 1;
	elementoZona.zone.poly.nArcs = 0;
	
	memoElementoZona.coords = (API_Coord**) BMAllocateHandle((elementoZona.zone.poly.nCoords + 1) * sizeof (API_Coord), ALLOCATE_CLEAR, 0);
	memoElementoZona.pends  = (Int32**) BMAllocateHandle ((elementoZona.zone.poly.nSubPolys + 1) * sizeof (Int32), ALLOCATE_CLEAR, 0);
	if (memoElementoZona.coords != NULL && memoElementoZona.pends != NULL) {
		(*memoElementoZona.coords)[1].x = xOrigin;
		(*memoElementoZona.coords)[1].y = yOrigin;

		(*memoElementoZona.coords)[2].x = xOrigin;
		(*memoElementoZona.coords)[2].y = yOrigin + elemento->GetY();

		(*memoElementoZona.coords)[3].x = xOrigin + elemento->GetX();
		(*memoElementoZona.coords)[3].y = yOrigin + elemento->GetY();

		(*memoElementoZona.coords)[4].x = xOrigin + elemento->GetX();
		(*memoElementoZona.coords)[4].y = yOrigin;

		(*memoElementoZona.coords)[5].x = (*memoElementoZona.coords)[1].x;
		(*memoElementoZona.coords)[5].y = (*memoElementoZona.coords)[1].y;

		(*memoElementoZona.pends)[0] = 0;
		(*memoElementoZona.pends)[1] = elementoZona.zone.poly.nCoords;
	}

	ACAPI_Element_Create(&elementoZona, &memoElementoZona);
	ACAPI_DisposeElemMemoHdls(&memoElementoZona);
}
