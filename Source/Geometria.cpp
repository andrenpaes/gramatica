/*
 *  Geometria.cpp
 *  Alvenaria
 *
 *  Created by André Rodrigues on 11/2/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */
#include "Geometria.h"

#include "math.h"

double arredondar(double value, double precision) {
	return floor((value / precision) + 0.5) * precision;
}

double Coordenada::distancia(const Coordenada &other) const {
	return arredondar(sqrt(pow(other.x - this->x, 2) + pow(other.y - this->y, 2)));
}

Coordenada Coordenada::operator=(const Coordenada& other) {
	if (this == &other) {
		return *this;
	}
	this->x = other.x;
	this->y = other.y;

	return *this;
}

bool Coordenada::operator==(const Coordenada& other) const {
	if (arredondar(this->x) == arredondar(other.x)
		&& arredondar(this->y) == arredondar(other.y)) {
		return true;
	}
	return false;
}

bool Coordenada::operator!=(const Coordenada& other) const {
	return !(*this == other);
}

Reta::Reta(double xi, double yi, double xf, double yf)
	: inicio(Coordenada(xi, yi)), fim(Coordenada(xf, yf)) {
}

double Reta::Comprimento() const {
	return inicio.distancia(fim);
}

bool Reta::operator==(const Reta& other) const {
	if ((this->inicio == other.inicio && this->fim == other.fim)
		|| (this->inicio == other.fim && this->fim == other.inicio)) {
		return true;
	}
	return false;
}

bool Reta::operator!=(const Reta& other) const {
	return !(*this == other);
}

VetorNormalizado2d::VetorNormalizado2d(double xi, double yi, double xf, double yf) {
	double comprimento = sqrt(pow(xf - xi, 2) + pow(yf - yi, 2));
	coordenada.SetX((xf - xi) / comprimento);
	coordenada.SetY((yf - yi) / comprimento);

	this->alpha = atan2(coordenada.GetY(), coordenada.GetX()) - atan2(0, 1);
}

void VetorNormalizado2d::rotacionar(double alpha) {
	const double x = coordenada.GetX(), y = coordenada.GetY();
	coordenada.SetX((x * cos(alpha)) - (y * sin(alpha)));
	coordenada.SetY((x * sin(alpha)) + (y * cos(alpha)));
	this->alpha += alpha;
}

VetorNormalizado2d VetorNormalizado2d::inverso() const {
	VetorNormalizado2d vetorInverso(*this);
	vetorInverso.rotacionar(M_PI);
	return vetorInverso;
}

VetorNormalizado2d VetorNormalizado2d::operator=(const VetorNormalizado2d& other) {
	if (this == &other) {
		return *this;
	}
	this->coordenada = other.coordenada;
	this->alpha = other.alpha;

	return *this;
}

bool VetorNormalizado2d::operator==(const VetorNormalizado2d& other) const {
	if (this->coordenada == other.coordenada) {
		return true;
	}
	return false;
}

bool VetorNormalizado2d::operator!=(const VetorNormalizado2d& other) const {
	return !(*this == other);
}
