//
//  EngineRubyGramatica.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 12/01/13.
//
//

#include "EngineRubyGramatica.h"
#include "Util.h"

#include "mruby.h"
#include "mruby/array.h"
#include "mruby/class.h"
#include "mruby/compile.h"
#include "mruby/data.h"
#include "mruby/string.h"
#include "mruby/variable.h"

#include <map>

std::map<mrb_state*, EngineRubyGramatica*> _engineMap;

mrb_value mrb_engine_gramatica_writer(mrb_state *mrb, mrb_value self) {
	mrb_value gramatica;
	mrb_get_args(mrb, "o", &gramatica);
	mrb_iv_set(mrb, self, mrb_intern(mrb, "@gramatica"), gramatica);

	return self;
}

mrb_value mrb_engine_testada(mrb_state *mrb, __unused mrb_value self) {
	EngineRubyGramatica *engine = _engineMap[mrb];

	return mrb_float_value(mrb, engine->GetParametrosLote()->GetTestada());
}

mrb_value mrb_engine_comprimento(mrb_state *mrb, __unused mrb_value self) {
	EngineRubyGramatica *engine = _engineMap[mrb];
	
	return mrb_float_value(mrb, engine->GetParametrosLote()->GetComprimento());
}

mrb_value mrb_engine_azimute(mrb_state *mrb, __unused mrb_value self) {
	EngineRubyGramatica *engine = _engineMap[mrb];
	
	return mrb_float_value(mrb, engine->GetParametrosLote()->GetAzimute());
}

mrb_value mrb_engine_tipo_lote(mrb_state *mrb, __unused mrb_value self) {
	EngineRubyGramatica *engine = _engineMap[mrb];
	
	return mrb_float_value(mrb, engine->GetParametrosLote()->GetTipoLote());
}

mrb_value mrb_engine_run(mrb_state *mrb, mrb_value self){
	EngineRubyGramatica *engine = _engineMap[mrb];
	mrb_value gramatica = mrb_iv_get(mrb, self, mrb_intern(mrb, "@gramatica"));

	mrb_funcall(mrb, gramatica, "processar", 0);

	mrb_value resultado = mrb_iv_get(mrb, gramatica, mrb_intern(mrb, "@resultado"));
	engine->ProcessResult(resultado);

	return mrb_nil_value();
}

static Elemento* criarElemento(mrb_state *mrb, mrb_value elemento) {
	mrb_value mrb_simbolo = mrb_funcall(mrb, elemento, "simbolo", 0);
	char *simbolo = mrb_string_value_cstr(mrb, &mrb_simbolo);

	mrb_value mrb_categoria = mrb_funcall(mrb, elemento, "categoria", 0);
	char *categoria = mrb_string_value_cstr(mrb, &mrb_categoria);

	const double x = mrb_float(mrb_funcall(mrb, elemento, "x", 0));
	const double y = mrb_float(mrb_funcall(mrb, elemento, "y", 0));

	return new Elemento(simbolo, categoria, x, y);
}

static std::vector<Node*>* criarElementos(mrb_state *mrb, mrb_value iteracao);

static Container* criarContainer(mrb_state *mrb, mrb_value mrb_container) {
	const double x = mrb_float(mrb_funcall(mrb, mrb_container, "x", 0));
	const double y = mrb_float(mrb_funcall(mrb, mrb_container, "y", 0));

	Container *container = new Container(x, y);
	std::vector<Node*> *subElementos =
		criarElementos(mrb, mrb_funcall(mrb, mrb_container, "sub_elementos", 0));
	container->AdicionarSubElementos(*subElementos);
	delete subElementos;
	
	return container;
}

static std::vector<Node*>* criarElementos(mrb_state *mrb, mrb_value iteracao) {
	std::vector<Node*> *elementos = new std::vector<Node*>();
	mrb_int numero_elementos = mrb_ary_len(mrb, iteracao);

	for (mrb_int i = 0; i < numero_elementos; i++) {
		mrb_value elemento = mrb_ary_entry(iteracao, i);
		Node *node;
		if (mrb_respond_to(mrb, elemento, mrb_intern(mrb, "sub_elementos"))) {
			node = criarContainer(mrb, elemento);
		} else {
			node = criarElemento(mrb, elemento);
		}
		elementos->push_back(node);
	}

	return elementos;
}


IEngineGramatica* CreateGramaticaEngine() {
	EngineRubyGramatica *engine = new EngineRubyGramatica;
	
	return engine;
}

EngineRubyGramatica::EngineRubyGramatica() {
	RClass *gramaticaModule = mrb_define_module(mrb, "Gramatica");
	RClass *engineClass = mrb_define_class_under(mrb, gramaticaModule, "Engine", mrb->object_class);

	mrb_define_method(mrb, engineClass, "gramatica=", mrb_engine_gramatica_writer, MRB_ARGS_REQ(1));
	mrb_define_method(mrb, engineClass, "testada", mrb_engine_testada, MRB_ARGS_NONE());
	mrb_define_method(mrb, engineClass, "comprimento", mrb_engine_comprimento, MRB_ARGS_NONE());
	mrb_define_method(mrb, engineClass, "azimute", mrb_engine_azimute, MRB_ARGS_NONE());
	mrb_define_method(mrb, engineClass, "tipo_lote", mrb_engine_tipo_lote, MRB_ARGS_NONE());

	mrb_define_method(mrb, engineClass, "run", mrb_engine_run, MRB_ARGS_NONE());

	_engineMap[mrb] = this;
}

IEngine* EngineRubyGramatica::GetIEngineInstance() {
	return this;
}

EngineRubyGramatica::~EngineRubyGramatica() {
	_engineMap.erase(mrb);
}

EngineError EngineRubyGramatica::LoadEngine() {
	static const int NUMBER_OF_FILES = 4;
	static const char* FILES[NUMBER_OF_FILES] = {"additions.rb", "dynamic_attributes.rb", "attributes.rb", "engine_gramatica.rb"};

	EngineError error = 0;
	for (int i = 0; i < NUMBER_OF_FILES; i++) {
		if ((error = ExecuteRubyFile(FILES[i]))) {
			return error;
		}
	}

	return error;
}

EngineError EngineRubyGramatica::Process(const ParametrosLote *parametros) {
	this->parametrosLote = parametros;
	return ExecuteRubyFile("gramatica.rb");
}

void EngineRubyGramatica::ProcessResult(mrb_value resultado) {
	std::vector<Node*> *elementos = criarElementos(mrb, resultado);
	Sequencia* sequencia = new Sequencia(*elementos);
	delete elementos;

	SetResultado(sequencia);
}

