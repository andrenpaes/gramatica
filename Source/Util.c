//
//  Util.c
//  Gramatica
//
//  Created by Andre Rodrigues on 07/03/13.
//
//

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

char* copy_cstring(const char* cstring, ...) {
	if (cstring == NULL) {
		return NULL;
	}

	va_list argList;
	va_start(argList, cstring);

	char buffer[1024];
	int len = vsnprintf(buffer, sizeof(buffer), cstring, argList);

	if (len == 0) {
		return NULL;
	}

	char* newString = (char*) calloc(len, sizeof(char));
	strcpy(newString, buffer);
		
	return newString;
}

char* append_to_path(const char* path, const char* fileName) {
	char* file_path = (char *) calloc((strlen(path) + strlen(fileName) + 1), sizeof(char));
	strcpy(file_path, path);
	strcat(file_path, "/");
	strcat(file_path, fileName);
	
	return file_path;
}

char* read_file(const char* filePath) {
	FILE* file = fopen(filePath, "rb");

	fseek(file, 0, SEEK_END);
	size_t size = ftell(file);

	char* data = (char*) calloc(size, sizeof(char));
	rewind(file);
	fread(data, sizeof(char), size, file);
	fclose(file);

	return data;
}
