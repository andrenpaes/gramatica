//
//  IEngineGramatica.h
//  Gramatica
//
//  Created by Andre Rodrigues on 10/01/13.
//
//

#ifndef Gramatica_IEngineGramatica_h
#define Gramatica_IEngineGramatica_h

#include "IEngine.h"
#include "ParametrosLote.h"

#include <vector>

struct Node {
	virtual ~Node() {};

	virtual double GetX() const = 0;
	virtual double GetY() const = 0;
};

class Elemento: public Node {
private:
	char* simbolo;
	char* categoria;
	const double x;
	const double y;
public:
	Elemento(const char* simbolo, const char* categoria, double x, double y);
	~Elemento();

	const char* GetSimbolo() const {
		return this->simbolo;
	};

	const char* GetCategoria() const {
		return this->categoria;
	};

	double GetX() const {
		return this->x;
	};
	double GetY() const {
		return this->y;
	};
};

class Container: public Node {
private:
	const double x;
	const double y;
	std::vector<Node*> subElementos;
public:
	Container(double x, double y): x(x), y(y) {};
	~Container() {
		while(!subElementos.empty()) {
			delete subElementos.back();
			subElementos.pop_back();
		}
	};

	double GetX() const {
		return this->x;
	};
	double GetY() const {
		return this->y;
	};
	const std::vector<Node*>& GetSubElementos() const {
		return subElementos;
	};

	void AdicionarSubElementos(const std::vector<Node*> &subElementos) {
		std::vector<Node*>::const_iterator it;
		for (it = subElementos.begin() ; it < subElementos.end(); it++ ) {
			this->subElementos.push_back(*it);
		}
	}
};

class Sequencia {
private:
	std::vector<Node*> elementos;
public:
	Sequencia(const std::vector<Node*> &elementos): elementos(elementos) {};
	~Sequencia() {
		while(!elementos.empty()) {
			delete elementos.back();
			elementos.pop_back();
		}
	};

	int GetQuantidadeElementos() const {
		return elementos.size();
	};

	const std::vector<Node*>& GetElementos() const {
		return elementos;
	};
};

struct IEngineGramatica : public IEngine {
protected:
	const ParametrosLote* parametrosLote;
	Sequencia* resultado;

	IEngineGramatica(): IEngine(), resultado(NULL) {};

	void SetResultado(Sequencia* resultado) {
		if (this->resultado != NULL) {
			delete this->resultado;
		}
		this->resultado = resultado;
	}
public:
	virtual ~IEngineGramatica();

	virtual EngineError Process(const ParametrosLote *parametros) = 0;

	const ParametrosLote* GetParametrosLote() const {
		return this->parametrosLote;
	}
	const Sequencia* GetResultado() const {
		return this->resultado;
	};
};

IEngineGramatica* CreateGramaticaEngine();

#endif
