//
//  GramaticaDialog.h
//  Gramatica
//
//  Created by Andre Rodrigues on 01/11/13.
//
//

#ifndef Gramatica_GramaticaDialog_h
#define Gramatica_GramaticaDialog_h

#include "ParametrosLote.h"

ParametrosLote* GerarParametrosLote();

#endif
