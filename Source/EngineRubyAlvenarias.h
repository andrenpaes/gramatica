//
//  EngineRubyAlvenarias.h
//  Gramatica
//
//  Created by Andre Rodrigues on 08/06/13.
//
//

#ifndef __Gramatica__EngineRubyAlvenarias__
#define __Gramatica__EngineRubyAlvenarias__

#include "IEngineAlvenarias.h"
#include "EngineRuby.h"

class EngineRubyAlvenarias: public IEngineAlvenarias, public EngineRuby {
private:
	mrb_value stateInstance;

	EngineError GerarParedes();
	EngineError GerarCobertas();
protected:
	IEngine* GetIEngineInstance();
public:
	EngineError LoadInfoZona(Zona& zona);

	EngineError LoadEngine();
	EngineError Process();
};

#endif /* defined(__Gramatica__EngineRubyAlvenarias__) */
