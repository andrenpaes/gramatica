//
//  GramaticaDialog.mm
//  Gramatica
//
//  Created by Andre Rodrigues on 01/11/13.
//
//

#import "GramaticaDialog.h"

#import "GramaticaDialogController.h"

ParametrosLote* GerarParametrosLote() {
	ParametrosLote* parametrosLote = new ParametrosLote();

	GramaticaAction return_value;
	@autoreleasepool {
		return_value = [GramaticaDialogController openDialog:parametrosLote];
	}

	if (return_value == ActionOk) {
		return parametrosLote;
	}

	delete parametrosLote;
	return NULL;
}
