/**
 * @file Contains the functions required by ArchiCAD.
 */

#include "Gramatica.h"

#include "GramaticaAttributes.h"

#include "GramaticaDialog.h"

#include "ParametrosLote.h"
#include "ConstrutorGramatica.h"
#include "ConstrutorAlvernarias.h"

#include "APICommon.h"

void GerarElementos() {
	ACAPI_KeepInMemory(true);

	ParametrosLote* parametrosLote = GerarParametrosLote();

	if (parametrosLote) {
		API_Coord clickedPoint;
		if (ClickAPoint("Origem para geração da planta", &clickedPoint)) {
			ConstrutorGramatica construtor;
			if (construtor.Processar(parametrosLote)) {
				WriteReport_Alert(construtor.GetDescricaoErro());
			} else {
				construtor.Construir(clickedPoint.x, clickedPoint.y);
			}
		}
		delete parametrosLote;
	}
}

void GerarParedes() {
    API_SelectionInfo selectionInfo;
    API_Neig **selNeigs;

    GSErrCode err = ACAPI_Selection_Get(&selectionInfo, &selNeigs, true);
    BMKillHandle((GSHandle *) &selectionInfo.marquee.coords);
    if (err == APIERR_NOSEL) {
        err = NoError;
	}
    if (err != NoError) {
        BMKillHandle((GSHandle *) &selNeigs);
        return;
    }

	if (selectionInfo.typeID == API_SelEmpty) {
		WriteReport_Alert("Selecione as zonas de layout do projeto.");
	} else {
		ConstrutorAlvernarias construtor;
		for (int elementIndex = 0; elementIndex < selectionInfo.sel_nElem; elementIndex++) {
			API_ElemTypeID selectedElementTypeID = Neig_To_ElemID ((*selNeigs)[elementIndex].neigID);
			if (selectedElementTypeID != API_ZoneID)
				continue;

			API_Element elementoZona;
			BNZeroMemory(&elementoZona, sizeof(API_Element));
			elementoZona.header.guid = (*selNeigs)[elementIndex].guid;

			ACAPI_Element_Get(&elementoZona);
			construtor.AdicionarZona(elementoZona);
		}
		if (construtor.Processar()) {
			WriteReport_Alert(construtor.GetDescricaoErro());
		} else {
			construtor.Construir();
		}
	}
	BMKillHandle((GSHandle *) &selNeigs);
}

GSErrCode __ACDLL_CALL MenuCommandHandler(const API_MenuParams *menuParams) {
	switch (menuParams->menuItemRef.menuResID) {
		case 32500:
			switch (menuParams->menuItemRef.itemIndex) {
				case 1:
					GerarElementos();
					break;
				case 2:
					GerarParedes();
					break;
			}
			break;
	}

	return NoError;
}		// MenuCommandHandler

/**
 * Dependency definitions.
 * @param envir [in] ArchiCAD environment values.
 * @return The Add-On loading type.
 */
API_AddonType __ACDLL_CALL CheckEnvironment(API_EnvirParams* envir) {

	GSResModule saveResModule = ACAPI_UseOwnResModule ();
	ACAPI_Resource_GetLocStr (envir->addOnInfo.name, 32000, 1);
	ACAPI_Resource_GetLocStr (envir->addOnInfo.description, 32000, 2);
	ACAPI_ResetResModule (saveResModule);

	return APIAddon_Normal;
}		// CheckEnvironment

/**
 * Interface definitions.
 * @return ArchiCAD error code.
 */
GSErrCode __ACDLL_CALL RegisterInterface(void) {
	GSErrCode err = NoError;
	err = ACAPI_Register_Menu (32500, 32501, MenuCode_UserDef, MenuFlag_SeparatorBefore);
	if (err == NoError) {
		err = ACAPI_Register_BuiltInLibrary();
	}

	return err;
}		// RegisterInterface

/**
 * Called after the Add-On has been loaded into memory.
 * @return ArchiCAD error code.
 */
GSErrCode __ACDLL_CALL Initialize(void) {
	GSErrCode err = ACAPI_Install_MenuHandler (32500, MenuCommandHandler);
	if (err != NoError) {
		DBPrintf ("Gramatica:: Initialize () ACAPI_Install_MenuHandler failed\n");
	}
	GramaticaAttributes &attributesManager = GramaticaAttributes::GetInstance();
	attributesManager.CriarAtributos();

	return err;
}		// Initialize

/**
 * Called when the Add-On is going to be unloaded.
 * @return ArchiCAD error code.
 */
GSErrCode __ACDLL_CALL FreeData(void) {
	GSErrCode err = NoError;

	return err;
}		// FreeData
