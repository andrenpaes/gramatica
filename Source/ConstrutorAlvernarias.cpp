//
//  ConstrutorAlvernarias.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 27/05/13.
//
//

#include "ConstrutorAlvernarias.h"
#include "APICommon.h"

#include "IEngineAlvenarias.h"
#include "EngineLoader.h"

Zona* construirZona(API_Element elementoZona) {
	API_ElementMemo elementMemo;
	BNZeroMemory(&elementMemo, sizeof(API_ElementMemo));
	ACAPI_Element_GetMemo(elementoZona.header.guid, &elementMemo);

	GramaticaAttributes &attributes = GramaticaAttributes::GetInstance();
	Zona* zona = new Zona(elementoZona.zone.roomName, attributes.NomeZona(elementoZona.zone.catInd));

	int numeroCoordendas = elementoZona.zone.poly.nCoords;
	for (int i = 1; i < numeroCoordendas; i++) {
		Coordenada coordenada((*elementMemo.coords)[i].x, (*elementMemo.coords)[i].y);
		zona->GetCoordenadas().push_back(coordenada);
	}

	ACAPI_DisposeElemMemoHdls(&elementMemo);

	return zona;
}

ConstrutorAlvernarias::ConstrutorAlvernarias(): attributes(GramaticaAttributes::GetInstance()) {
	engine = CreateAlvenariasEngine();
}

ConstrutorAlvernarias::~ConstrutorAlvernarias() {
	delete engine;
}

void ConstrutorAlvernarias::AdicionarZona(API_Element elementoZona) {
	zonas.push_back(elementoZona);
}

int ConstrutorAlvernarias::Processar() {
	if (int erro = Inicializar(engine)) {
		return erro;
	}

	std::vector<API_Element>::iterator zonas;
	for (zonas = this->zonas.begin(); zonas < this->zonas.end(); zonas++) {
		API_Element elementoZona = *zonas;
		Zona *zona = construirZona(elementoZona);

		EngineError error = engine->LoadInfoZona(*zona);
		delete zona;
		if (error) {
			return error;
		}
	}

	if (int erro = engine->Process()) {
		return erro;
	}

	return 0;
}

const char* ConstrutorAlvernarias::GetDescricaoErro() const {
	return engine->GetErrorDescription();
}

GSErrCode getLibraryPart(const char* name, API_LibPart* libraryPart) {
	BNZeroMemory(libraryPart, sizeof(API_LibPart));
	CHANSI2Unicode(name, GS::CStringLen, libraryPart->docu_UName, API_UniLongNameLen);

	return ACAPI_LibPart_Search(libraryPart, false);
}

GSErrCode changeElementParameter(API_ElemTypeID elementType,
								 long elementIndex, const char* name, double value) {
    API_ParamOwnerType paramOwner;
    BNZeroMemory(&paramOwner, sizeof (API_ParamOwnerType));
    paramOwner.libInd = 0;
    paramOwner.typeID = elementType;
    paramOwner.index = elementIndex;

	API_GetParamsType getParams;
    BNZeroMemory (&getParams, sizeof (API_GetParamsType));

    GSErrCode err = ACAPI_Goodies (APIAny_OpenParametersID, &paramOwner, NULL);
    if (err == NoError) {
		API_ChangeParamType chgParam;
        BNZeroMemory (&chgParam, sizeof (API_ChangeParamType));

        err = ACAPI_Goodies (APIAny_GetActParametersID, &getParams, NULL);
        if (err == NoError) {
            chgParam.index = 0;
            CHCopyC(name, chgParam.name);
            chgParam.realValue = value;
            err = ACAPI_Goodies (APIAny_ChangeAParameterID, &chgParam, NULL);
            if (err == NoError) {
                err = ACAPI_Goodies (APIAny_GetActParametersID, &getParams, NULL);
            }
        }
        ACAPI_Goodies (APIAny_CloseParametersID, NULL, NULL);
    }

    if (err == NoError) {
		API_Element element;
        BNZeroMemory (&element, sizeof (API_Element));
		element.header.typeID = elementType;
        element.header.index  = elementIndex;

		API_ElementMemo memo;
        BNZeroMemory (&memo, sizeof (API_ElementMemo));
		memo.params = getParams.params;

		API_Element mask;
        ACAPI_ELEMENT_MASK_CLEAR(mask);
        ACAPI_Element_Change(&element, &mask, &memo, APIMemoMask_AddPars, true);
    }
    ACAPI_DisposeAddParHdl (&getParams.params);
	
    return err;
}

void construirJanela(Parede& parede, Abertura& abertura, API_Element* owner) {
	API_Element elementoJanela;
	BNZeroMemory(&elementoJanela, sizeof(API_Element));
	API_ElementMemo memoElementoJanela;
	BNZeroMemory(&memoElementoJanela, sizeof(API_ElementMemo));

	elementoJanela.header.typeID = API_WindowID;
	ACAPI_Element_GetDefaults(&elementoJanela, &memoElementoJanela);

	elementoJanela.window.owner = owner->header.guid;
	elementoJanela.window.lower = abertura.GetPeitoril();

	double distanciaOrigem = parede.GetInicio().distancia(abertura.GetRetaReferencia().GetInicio()) + abertura.GetDistanciaOrigem();
	elementoJanela.window.objLoc = distanciaOrigem;

	ACAPI_Element_Create(&elementoJanela, &memoElementoJanela);
	ACAPI_DisposeElemMemoHdls(&memoElementoJanela);

	changeElementParameter(elementoJanela.header.typeID, elementoJanela.header.index, "A", arredondar(abertura.GetArea() / abertura.GetAltura(), 0.05));
	changeElementParameter(elementoJanela.header.typeID, elementoJanela.header.index, "B", abertura.GetAltura());
}

void construirPorta(Parede& parede, Abertura& abertura, API_Element* owner) {
	API_Element elementoPorta;
	BNZeroMemory(&elementoPorta, sizeof(API_Element));
	API_ElementMemo memoElementoPorta;
	BNZeroMemory(&memoElementoPorta, sizeof(API_ElementMemo));

	elementoPorta.header.typeID = API_DoorID;
	ACAPI_Element_GetDefaults(&elementoPorta, &memoElementoPorta);
	elementoPorta.door.owner = owner->header.guid;
	double distanciaOrigem = parede.GetInicio().distancia(abertura.GetRetaReferencia().GetInicio()) + abertura.GetDistanciaOrigem();
	elementoPorta.door.objLoc = distanciaOrigem + (abertura.GetComprimento() / 2);

	API_LibPart porta;
	if (CHEqualCStrings(abertura.GetTipo(), "vao")) {
		getLibraryPart("D Rectangular Opening 14", &porta);
	} else if (abertura.GetComprimento() == 1.2) {
		getLibraryPart("D1 Transom 14", &porta);
	} else {
		getLibraryPart("D1 14", &porta);
	}
	elementoPorta.door.libInd = porta.index;

	ACAPI_Element_Create(&elementoPorta, &memoElementoPorta);
	ACAPI_DisposeElemMemoHdls(&memoElementoPorta);

	changeElementParameter(elementoPorta.header.typeID, elementoPorta.header.index, "A", abertura.GetComprimento());
	changeElementParameter(elementoPorta.header.typeID, elementoPorta.header.index, "B", abertura.GetAltura());
}

void construirAberturas(Parede& parede, API_Element* elementoParede) {
	std::vector<Abertura*> aberturas = parede.GetAberturas();
	std::vector<Abertura*>::const_iterator it;
	for (it = aberturas.begin(); it < aberturas.end(); it++) {
		Abertura* abertura = *it;

		if (CHEqualCStrings(abertura->GetTipo(), "janela")) {
			construirJanela(parede, *abertura, elementoParede);
		} else {
			construirPorta(parede, *abertura, elementoParede);
		}
	}
}

void ConstrutorAlvernarias::Construir() const {
	ACAPI_OpenUndoableSession("Construir!");
	ConstruirParedes();
	ConstruirLaje();
	ConstruirCoberta();
	ACAPI_CloseUndoableSession();
}

void ConstrutorAlvernarias::ConstruirParedes() const {
	const std::vector<Parede*> paredes = engine->GetResultadoParedes();

	std::vector<Parede*>::const_iterator it;
	for (it = paredes.begin(); it < paredes.end(); it++) {
		Parede* parede = *it;
		
		API_Element elementoParede;
		BNZeroMemory(&elementoParede, sizeof(API_Element));
		API_ElementMemo memoElementoParede;
		BNZeroMemory(&memoElementoParede, sizeof(API_ElementMemo));
		
		elementoParede.header.typeID = API_WallID;
		ACAPI_Element_GetDefaults(&elementoParede, &memoElementoParede);
		
		elementoParede.header.layer = attributes.IndexLayerAlvenaria();
		elementoParede.wall.type = APIWtyp_Normal;
		elementoParede.wall.dir = APIWdir_Center;
		elementoParede.wall.bottom = 0;
		elementoParede.wall.top = parede->GetAltura();
		elementoParede.wall.thickness = parede->GetEspessura();
		
		API_Coord inicio;
		inicio.x = parede->GetInicio().GetX(), inicio.y = parede->GetInicio().GetY();
		elementoParede.wall.begC = inicio;
		
		API_Coord fim;
		fim.x = parede->GetFim().GetX(), fim.y = parede->GetFim().GetY();
		elementoParede.wall.endC = fim;
		
		ACAPI_Element_Create(&elementoParede, &memoElementoParede);
		ACAPI_DisposeElemMemoHdls(&memoElementoParede);
		
		construirAberturas(*parede, &elementoParede);
	}
}

GSErrCode construirElementoLaje(API_Element elementoZona, short layerIndex) {
	API_Element elementoLaje;
	BNZeroMemory(&elementoLaje, sizeof(API_Element));
	API_ElementMemo memoElementoLaje;
	BNZeroMemory(&memoElementoLaje, sizeof(API_ElementMemo));
	
	elementoLaje.header.typeID = API_SlabID;
	ACAPI_Element_GetDefaults(&elementoLaje, &memoElementoLaje);

	elementoLaje.header.layer = layerIndex;
	elementoLaje.slab.thickness = GramaticaAttributes::GetInstance().GetEspessuraLaje();
	elementoLaje.slab.level = GramaticaAttributes::GetInstance().GetAlturaParede() + elementoLaje.slab.thickness;

	int numberOfCoords = elementoZona.zone.poly.nCoords;
	elementoLaje.slab.poly.nCoords = numberOfCoords;
	elementoLaje.slab.poly.nSubPolys = 1;
	elementoLaje.slab.poly.nArcs = 0;

	API_ElementMemo memoElementoZona;
	BNZeroMemory(&memoElementoZona, sizeof (API_ElementMemo));
	ACAPI_Element_GetMemo(elementoZona.header.guid, &memoElementoZona);

	memoElementoLaje.coords = (API_Coord**) BMAllocateHandle((elementoLaje.slab.poly.nCoords + 1) * sizeof (API_Coord), ALLOCATE_CLEAR, 0);
	memoElementoLaje.pends  = (Int32**) BMAllocateHandle((elementoLaje.slab.poly.nSubPolys + 1) * sizeof (Int32), ALLOCATE_CLEAR, 0);
	memoElementoLaje.vertexIDs  = (UInt32**) BMAllocateHandle((elementoLaje.slab.poly.nCoords + 1) * sizeof (Int32), ALLOCATE_CLEAR, 0);
	if (memoElementoLaje.coords != NULL && memoElementoLaje.pends != NULL && memoElementoLaje.vertexIDs != NULL) {
		(*memoElementoLaje.coords)[0].x = 0.0;
		(*memoElementoLaje.coords)[0].y = 0.0;
		(*memoElementoLaje.vertexIDs)[0] = numberOfCoords - 1;

		for (int vertex = 1; vertex <= elementoLaje.slab.poly.nCoords; vertex++) {
			(*memoElementoLaje.coords)[vertex].x = (*memoElementoZona.coords)[vertex].x;
			(*memoElementoLaje.coords)[vertex].y = (*memoElementoZona.coords)[vertex].y;
			(*memoElementoLaje.vertexIDs)[vertex] = vertex;
		}
		(*memoElementoLaje.vertexIDs)[numberOfCoords] = 1;

		(*memoElementoLaje.pends)[0] = 0;
		(*memoElementoLaje.pends)[1] = numberOfCoords;
	}

	GSErrCode err = ACAPI_Element_Create(&elementoLaje, &memoElementoLaje);
	if (err) {
		WriteReport_Alert("ERRO! %i", err - APIErrorStart);
	}
	ACAPI_DisposeElemMemoHdls(&memoElementoLaje);
	ACAPI_DisposeElemMemoHdls(&memoElementoZona);

	return err;
}

void ConstrutorAlvernarias::ConstruirLaje() const {
	std::vector<API_Element>::const_iterator zonas;
	for (zonas = this->zonas.begin(); zonas < this->zonas.end(); zonas++) {
		API_Element elementoZona = *zonas;
		if (CHEqualCStrings("Area Externa", attributes.NomeZona(elementoZona.zone.catInd))
			|| CHEqualCStrings("Muro Externo", attributes.NomeZona(elementoZona.zone.catInd))
			|| CHEqualCStrings("Lavanderia", attributes.NomeZona(elementoZona.zone.catInd))) {
			continue;
		}
		if (construirElementoLaje(elementoZona, attributes.IndexLayerAlvenaria())) {
			break;
		}
	}
}

GSErrCode construirElementoCoberta(Coberta& coberta, short layerIndex) {
	API_Element elementoCoberta;
	BNZeroMemory(&elementoCoberta, sizeof(API_Element));
	API_ElementMemo memoElementoCoberta;
	BNZeroMemory(&memoElementoCoberta, sizeof(API_ElementMemo));

	elementoCoberta.header.typeID = API_RoofID;
	ACAPI_Element_GetDefaults(&elementoCoberta, &memoElementoCoberta);

	elementoCoberta.header.layer = layerIndex;
	elementoCoberta.roof.angle = coberta.GetAnguloInclinacao();
	elementoCoberta.roof.level = coberta.GetAltura();
	elementoCoberta.roof.thickness = coberta.GetEspessura();
	elementoCoberta.roof.sideType = APIRside_Vertical;

	API_Sector baseline;
	baseline.c1.x = coberta.GetBaseline().GetInicio().GetX();
	baseline.c1.y = coberta.GetBaseline().GetInicio().GetY();
	baseline.c2.x = coberta.GetBaseline().GetFim().GetX();
	baseline.c2.y = coberta.GetBaseline().GetFim().GetY();
	elementoCoberta.roof.baseLine = baseline;
	elementoCoberta.roof.posSign = coberta.GetSinalPivo();

	int numberOfCoords = coberta.GetPontos().size();
	elementoCoberta.roof.poly.nCoords = numberOfCoords;
	elementoCoberta.roof.poly.nSubPolys = 1;
	elementoCoberta.roof.poly.nArcs = 0;

	memoElementoCoberta.coords = (API_Coord**) BMAllocateHandle((elementoCoberta.roof.poly.nCoords + 1) * sizeof (API_Coord), ALLOCATE_CLEAR, 0);
	memoElementoCoberta.pends  = (Int32**) BMAllocateHandle((elementoCoberta.roof.poly.nSubPolys + 1) * sizeof (Int32), ALLOCATE_CLEAR, 0);
	memoElementoCoberta.vertexIDs  = (UInt32**) BMAllocateHandle((elementoCoberta.roof.poly.nCoords + 1) * sizeof (Int32), ALLOCATE_CLEAR, 0);
	if (memoElementoCoberta.coords != NULL && memoElementoCoberta.pends != NULL && memoElementoCoberta.vertexIDs != NULL) {
		(*memoElementoCoberta.coords)[0].x = 0.0;
		(*memoElementoCoberta.coords)[0].y = 0.0;
		(*memoElementoCoberta.vertexIDs)[0] = numberOfCoords - 1;

		for (int vertex = 1; vertex <= numberOfCoords; vertex++) {
			(*memoElementoCoberta.coords)[vertex].x = coberta.GetPontos()[vertex - 1].GetX();
			(*memoElementoCoberta.coords)[vertex].y = coberta.GetPontos()[vertex - 1].GetY();
			(*memoElementoCoberta.vertexIDs)[vertex] = vertex;
		}
		(*memoElementoCoberta.vertexIDs)[numberOfCoords] = 1;

		(*memoElementoCoberta.pends)[0] = 0;
		(*memoElementoCoberta.pends)[1] = numberOfCoords;
	}

	GSErrCode err = ACAPI_Element_Create(&elementoCoberta, &memoElementoCoberta);
	if (err) {
		WriteReport_Alert("ERRO! %i", err - APIErrorStart);
	}
	ACAPI_DisposeElemMemoHdls(&memoElementoCoberta);
	
	return err;
}

void ConstrutorAlvernarias::ConstruirCoberta() const {
	const std::vector<Coberta*> cobertas = engine->GetResultadoCobertas();

	std::vector<Coberta*>::const_iterator it;
	for (it = cobertas.begin(); it < cobertas.end(); it++) {
		Coberta* coberta = *it;
		if (construirElementoCoberta(*coberta, attributes.IndexLayerAlvenaria())) {
			break;
		}
	}
}
