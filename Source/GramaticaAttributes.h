//
//  GramaticaAttributes.h
//  Gramatica
//
//  Created by Andre Rodrigues on 25/05/13.
//
//

#ifndef __Gramatica__GramaticaAttributes__
#define __Gramatica__GramaticaAttributes__

#include "Attributes.h"

#include <functional>
#include <cstring>
#include <map>


struct StrCompare : public std::binary_function<const char*, const char*, bool> {
public:
    bool operator() (const char* str1, const char* str2) const {
		return std::strcmp(str1, str2) < 0;
	}
};
typedef std::map<const char*, short, StrCompare> ZoneMap;

class GramaticaAttributes {
private:
	GramaticaAttributes(): indexLayerZonas(0), indexLayerAlvenaria(0) {};

	// Stop the compiler generating methods of copy the object
	GramaticaAttributes(const GramaticaAttributes &copy); // Not Implemented
	GramaticaAttributes& operator=(const GramaticaAttributes &copy); // Not Implemented

	short indexLayerZonas;
	short indexLayerAlvenaria;
	ZoneMap zoneCategories;

	const double alturaParede = 2.5, espessuraLaje = 0.1;
public:
	static GramaticaAttributes& GetInstance() {
		static GramaticaAttributes instance;
		return instance;
	}

	void CriarAtributos();
	short IndexZona(const char* zona) const;
	const char* NomeZona(short indexZona) const;

	short IndexLayerZonas() const {
		return indexLayerZonas;
	}
	short IndexLayerAlvenaria() const {
		return indexLayerAlvenaria;
	}

	double GetAlturaParede() const {
		return alturaParede;
	}

	double GetEspessuraLaje() const {
		return espessuraLaje;
	}
};

#endif /* defined(__Gramatica__GramaticaAttributes__) */
