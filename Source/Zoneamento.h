//
//  Zoneamento.h
//  Gramatica
//
//  Created by Andre Rodrigues on 26/11/13.
//
//

#import <Foundation/Foundation.h>
#include "ParametrosLote.h"

@interface Zoneamento : NSObject
{
@private
	NSString* nome;
	double taxaPermeabilidade;
	double taxaOcupacao;
	double areaMinima;
	double testadaMinima;
	double profundidade;
}
@property (nonatomic, copy) NSString* nome;
@property (nonatomic) double taxaPermeabilidade;
@property (nonatomic) double taxaOcupacao;
@property (nonatomic) double areaMinima;
@property (nonatomic) double testadaMinima;
@property (nonatomic) double profundidade;

+ (NSArray*)loadZoneamentos;

- (NSArray*)validarParametros:(const ParametrosLote*)parametros;

- (double)calcularRecuoFrente:(const ParametrosLote*)parametros;
- (double)calcularRecuoFundo:(const ParametrosLote*)parametros;

@end
