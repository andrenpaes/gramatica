//
//  LoteView.m
//  Gramatica
//
//  Created by Andre Rodrigues on 01/11/13.
//
//

#import "LoteView.h"

@interface LoteView ()
@end

@implementation LoteView

@synthesize zoneamento;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		parametrosLote = NULL;
	}
    return self;
}

- (void)setParametrosLote:(const ParametrosLote*)parametros
{
	self->parametrosLote = parametros;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];
	[self limparCanvas];

	if (parametrosLote && parametrosLote->isDimensoesPreenchidas()) {
		float metrosTela;
		float pixelsTela;
		if (parametrosLote->GetTestada() > parametrosLote->GetComprimento()) {
			metrosTela = parametrosLote->GetTestada() * 8/5;
			pixelsTela = self.bounds.size.width;
		} else {
			metrosTela = parametrosLote->GetComprimento() * 8/5;
			pixelsTela = self.bounds.size.height;
		}

		float pixelsTestada = (parametrosLote->GetTestada() * pixelsTela) / metrosTela;
		float pixelsComprimento = (parametrosLote->GetComprimento() * pixelsTela) / metrosTela;
		float pixelsRecuoFrente = ([zoneamento calcularRecuoFrente:parametrosLote] * pixelsTela) / metrosTela;
		float pixelsRecuoFundo = ([zoneamento calcularRecuoFundo:parametrosLote] * pixelsTela) / metrosTela;

		float origemX = (self.bounds.size.width - pixelsTestada) / 2;
		float origemY = (self.bounds.size.height - pixelsComprimento) / 2;

		[[NSColor colorWithSRGBRed:0 green:0 blue:0 alpha:0.3] setFill];
		NSRectFillUsingOperation(NSMakeRect(origemX, origemY, pixelsTestada, pixelsComprimento), NSCompositeSourceOver);
		[[NSColor colorWithSRGBRed:0 green:0 blue:0 alpha:0.3] setFill];
		NSRectFillUsingOperation(NSMakeRect(origemX, origemY + pixelsRecuoFrente, pixelsTestada, pixelsComprimento - (pixelsRecuoFundo + pixelsRecuoFrente)), NSCompositeSourceOver);

		[[NSColor blackColor] setStroke];
		float dash[] = {4.0, 2.0};
		
		NSBezierPath* recuoFrentePath = [NSBezierPath bezierPath];
		[recuoFrentePath moveToPoint:NSMakePoint(origemX, origemY + pixelsRecuoFrente)];
		[recuoFrentePath lineToPoint:NSMakePoint(origemX + pixelsTestada, origemY + pixelsRecuoFrente)];
		[recuoFrentePath setLineDash:dash count:2 phase:0];
		[recuoFrentePath stroke];

		NSBezierPath* recuoFundoPath = [NSBezierPath bezierPath];
		[recuoFundoPath moveToPoint:NSMakePoint(origemX, origemY + pixelsComprimento - pixelsRecuoFundo)];
		[recuoFundoPath lineToPoint:NSMakePoint(origemX + pixelsTestada, origemY + pixelsComprimento - pixelsRecuoFundo)];
		[recuoFundoPath setLineDash:dash count:2 phase:0];
		[recuoFundoPath stroke];

		NSMutableParagraphStyle *style = [[[NSParagraphStyle defaultParagraphStyle] mutableCopy] autorelease];
		[style setAlignment:NSCenterTextAlignment];
		NSMutableDictionary *attr = [NSDictionary dictionaryWithObject:style forKey:NSParagraphStyleAttributeName];

		NSAffineTransform* azimuteTransform = [NSAffineTransform transform];
		[azimuteTransform translateXBy:self.bounds.size.width * 0.8 yBy:self.bounds.size.height * 0.85];
		[azimuteTransform rotateByDegrees:-parametrosLote->GetAzimute()];
		[azimuteTransform concat];
		[NSBezierPath strokeLineFromPoint:NSZeroPoint toPoint:NSMakePoint(30, 0)];
		[NSBezierPath strokeLineFromPoint:NSMakePoint(25, -5) toPoint:NSMakePoint(30, 0)];
		[NSBezierPath strokeLineFromPoint:NSMakePoint(25, 5) toPoint:NSMakePoint(30, 0)];

		NSAffineTransform* norteTransform = [NSAffineTransform transform];
		[norteTransform translateXBy:32 yBy:0];
		[norteTransform rotateByDegrees:parametrosLote->GetAzimute()];
		[norteTransform concat];
		[@"N" drawAtPoint:NSMakePoint(3, 3) withAttributes:attr];
		[norteTransform invert];
		[norteTransform concat];

		[azimuteTransform invert];
		[azimuteTransform concat];

		NSString *labelFrente = @"FT";
		NSString *labelEsquerda = @"LT1";
		NSString *labelDireita = @"LT2";
		NSString *labelFundo = @"FD";
		if (parametrosLote->GetTipoLote() == ESQUINA) {
			labelFrente = @"FT1";
			labelEsquerda = @"LT";
			labelDireita = @"FT2";
		}

		[labelFrente drawInRect:NSMakeRect(origemX, origemY - 25, pixelsTestada, 20) withAttributes:attr];
		[labelFundo drawInRect:NSMakeRect(origemX, origemY + pixelsComprimento + 5, pixelsTestada, 20) withAttributes:attr];

		NSAffineTransform* xform = [NSAffineTransform transform];
		[xform translateXBy:origemX + pixelsTestada + 25 yBy:origemY];
		[xform rotateByDegrees:90];
		[xform concat];
		[labelDireita drawInRect:NSMakeRect(0, 0, pixelsComprimento, 20) withAttributes:attr];
		[xform invert];
		[xform concat];

		xform = [NSAffineTransform transform];
		[xform translateXBy:origemX yBy:origemY];
		[xform rotateByDegrees:90];
		[xform concat];
		[labelEsquerda drawInRect:NSMakeRect(0, 0, pixelsComprimento, 20) withAttributes:attr];
		[xform invert];
		[xform concat];
	}
}

- (void)limparCanvas
{
	[[NSColor whiteColor] set];
	NSRectFill([self bounds]);
}

- (void)dealloc
{
	[super dealloc];
}

@end
