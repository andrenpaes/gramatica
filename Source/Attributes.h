//
//  Attributes.h
//  Gramatica
//
//  Created by Andre Rodrigues on 29/06/13.
//
//

#ifndef Gramatica_Attributes_h
#define Gramatica_Attributes_h

typedef enum {
	AREA_EXTERNA = 0xC6FE68,
	MURO_EXTERNO = 0xC6FE68,
	DORMITORIO = 0xFF8A43,
	ESTAR = 0x7BFFB6,
	CIRCULACAO = 0x79A3F8,
	COZINHA = 0xFFFF00,
	LAVANDERIA = 0xFFAB00,
	BANHEIRO = 0xFF00FF,
} CategoriaZona;

#endif
