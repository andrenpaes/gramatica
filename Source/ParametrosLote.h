//
//  ParametrosLote.h
//  Gramatica
//
//  Created by Andre Rodrigues on 01/11/13.
//
//

#ifndef Gramatica_ParametrosLote_h
#define Gramatica_ParametrosLote_h

enum TipoLote {
	NENHUM = -1,
	MEIO_DE_QUADRA = 0,
	ESQUINA = 1
};

class ParametrosLote {
private:
	double testada;
	double comprimento;
	double azimute;
	TipoLote tipoLote;

public:
	ParametrosLote(): testada(0), comprimento(0), azimute(0), tipoLote(MEIO_DE_QUADRA) {}

	void SetTestada(double testada) {
		this->testada = testada;
	}

	double GetTestada() const {
		return testada;
	}

	void SetComprimento(double comprimento) {
		this->comprimento = comprimento;
	}
	
	double GetComprimento() const {
		return comprimento;
	}

	void SetAzimute(double azimute) {
		this->azimute = azimute;
	}

	double GetAzimute() const {
		return azimute;
	}

	void SetTipoLote(TipoLote tipoLote) {
		this->tipoLote = tipoLote;
	}
	
	TipoLote GetTipoLote() const {
		return tipoLote;
	}

	bool isDimensoesPreenchidas() const {
		return testada && comprimento;
	}

	double Area() const {
		return testada * comprimento;
	}
};

#endif
