//
//  GramaticaAttributes.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 25/05/13.
//
//

#include "GramaticaAttributes.h"

#include "ACAPinc.h"


short CriarLayer(const char* nome) {
	API_Attribute attrib;
	BNZeroMemory (&attrib, sizeof (API_Attribute));
	API_AttributeDef defs;
	BNZeroMemory (&defs, sizeof (API_AttributeDef));

	attrib.header.typeID = API_LayerID;
	strcpy(attrib.layer.head.name, nome);
	
	ACAPI_Attribute_Create (&attrib, &defs);
	ACAPI_DisposeAttrDefsHdls (&defs);
	
	return attrib.header.index;
}

API_RGBColor CorParaZona(CategoriaZona zona) {
	API_RGBColor color;
	color.f_red = ((zona >> 16) & 0xFF) / 255.0;
	color.f_green = ((zona >> 8) & 0xFF) / 255.0;
	color.f_blue = (zona & 0xFF) / 255.0;

	return color;
}

short CriarCategoriaZona(CategoriaZona zona, const char* name) {
	API_Attribute attrib;
	BNZeroMemory (&attrib, sizeof (API_Attribute));
	API_AttributeDef defs;
	BNZeroMemory (&defs, sizeof (API_AttributeDef));

	attrib.header.typeID = API_ZoneCatID;
	strcpy(attrib.zoneCat.head.name, name);
	strcpy(attrib.zoneCat.stampName, "Zone_Stamp_1 14");
	attrib.zoneCat.rgb = CorParaZona(zona);

	API_AddParType** addPars = reinterpret_cast<API_AddParType**>(BMAllocateHandle(sizeof(API_AddParType), ALLOCATE_CLEAR, 0));
	defs.zone_addParItems = addPars;

	ACAPI_Attribute_Create(&attrib, &defs);
	ACAPI_DisposeAttrDefsHdls(&defs);

	return attrib.header.index;
}

void GramaticaAttributes::CriarAtributos() {
	this->indexLayerZonas = CriarLayer("Gramatica - Layout");
	this->indexLayerAlvenaria = CriarLayer("Gramatica - Alvenaria");

	zoneCategories["Dormitorio"] = CriarCategoriaZona(DORMITORIO, "Dormitorio");
	zoneCategories["Estar"] = CriarCategoriaZona(ESTAR, "Estar");
	zoneCategories["Circulacao"] = CriarCategoriaZona(CIRCULACAO, "Circulacao");
	zoneCategories["Area Externa"] = CriarCategoriaZona(AREA_EXTERNA, "Area Externa");
	zoneCategories["Muro Externo"] = CriarCategoriaZona(MURO_EXTERNO, "Muro Externo");
	zoneCategories["Cozinha"] = CriarCategoriaZona(COZINHA, "Cozinha");
	zoneCategories["Lavanderia"] = CriarCategoriaZona(LAVANDERIA, "Lavanderia");
	zoneCategories["Banheiro"] = CriarCategoriaZona(BANHEIRO, "Banheiro");
}

short GramaticaAttributes::IndexZona(const char* zona) const {
	const ZoneMap::const_iterator it = zoneCategories.find(zona);
	if (it != zoneCategories.end()) {
		return it->second;
	}

	return zoneCategories.at("Area Externa");
}

const char* GramaticaAttributes::NomeZona(short indexZona) const {
	ZoneMap::const_iterator iter;
	for (iter = zoneCategories.begin(); iter != zoneCategories.end(); iter++) {
		if (iter->second == indexZona) {
			return iter->first;
		}
	}

	return NULL;
}
