//
//  Zoneamento.m
//  Gramatica
//
//  Created by Andre Rodrigues on 26/11/13.
//
//

#import "Zoneamento.h"

@implementation Zoneamento

@synthesize nome;
@synthesize taxaPermeabilidade;
@synthesize taxaOcupacao;
@synthesize areaMinima;
@synthesize testadaMinima;
@synthesize profundidade;

+ (NSArray*)loadZoneamentos
{
	NSString *zoneamentosFilePath = [[NSBundle bundleWithIdentifier:@"com.graphisoft.Gramatica"]
									 pathForResource:@"zoneamento" ofType:@"plist"];
	NSData *zoneamentosXML = [[NSFileManager defaultManager] contentsAtPath:zoneamentosFilePath];
	NSArray *dadosZoneamentos =
		(NSArray*) [NSPropertyListSerialization propertyListWithData:zoneamentosXML
															 options:NSPropertyListImmutable
															  format:nil error:nil];

	NSMutableArray *zoneamentos = [NSMutableArray array];
	for (id value in dadosZoneamentos) {
		Zoneamento *zoneamento = [[[Zoneamento alloc] init] autorelease];
		[zoneamento setValuesForKeysWithDictionary:value];
		[zoneamentos addObject:zoneamento];
	}

	[zoneamentos sortUsingDescriptors:[NSArray arrayWithObjects:
									   [NSSortDescriptor sortDescriptorWithKey:@"nome" ascending:YES], nil]];
	return zoneamentos;
}

- (id)init
{
	self = [super init];
	if (self) {
	}
	return self;
}

- (NSArray*)validarParametros:(const ParametrosLote*)parametros
{
	NSMutableArray *erros = [NSMutableArray array];
	if (![self validarTestada:parametros->GetTestada()]) {
		[erros addObject:[NSString stringWithFormat:@"A testada não deve ser menor que %3.2f", testadaMinima]];
	}
	if (![self validarComprimento:parametros->GetComprimento()]) {
		[erros addObject:[NSString stringWithFormat:@"O comprimento não deve ser menor que %3.2f", profundidade]];
	}
	if (![self validarArea:parametros->Area()]) {
		[erros addObject:[NSString stringWithFormat:@"A area não deve ser menor que %3.2f", areaMinima]];
	}

	return erros;
}

- (BOOL)validarTestada:(double)testada
{
	return testada >= testadaMinima;
}

- (BOOL)validarComprimento:(double)comprimento
{
	return comprimento >= profundidade;
}

- (BOOL)validarArea:(double)area
{
	return area >= areaMinima;
}

- (double)calcularRecuoFrente:(const ParametrosLote*)parametros
{
	double unidadeRecuo = [self unidadeRecuo:parametros];
	if (3.0 * unidadeRecuo > 3.0) {
		return 3.0 * unidadeRecuo;
	}
	return 3.0;
}

- (double)calcularRecuoFundo:(const ParametrosLote*)parametros
{
	double unidadeRecuo = [self unidadeRecuo:parametros];
	if (4.0 * unidadeRecuo > 4.0) {
		return 4.0 * unidadeRecuo;
	}
	return 4.0;
}

- (double)unidadeRecuo:(const ParametrosLote*)parametros
{
	return (parametros->GetComprimento() * taxaPermeabilidade) / 7.0;
}

- (void)dealloc
{
	[nome release];

    [super dealloc];
}

@end
