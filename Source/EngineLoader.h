//
//  GramaticaDialog.h
//  Gramatica
//
//  Created by Andre Rodrigues on 19/10/12.
//
//

#ifndef __Gramatica__EngineLoader__
#define __Gramatica__EngineLoader__

#import "IEngine.h"

EngineError Inicializar(IEngine *engine);

#endif /* defined(__Gramatica__EngineLoader__) */
