//
//  LoteView.h
//  Gramatica
//
//  Created by Andre Rodrigues on 01/11/13.
//
//

#import <Cocoa/Cocoa.h>
#import "Zoneamento.h"
#include "ParametrosLote.h"

@interface LoteView : NSView
{
@private
	const ParametrosLote* parametrosLote;
	Zoneamento* zoneamento;
}
@property (nonatomic, retain) Zoneamento* zoneamento;

- (void)setParametrosLote:(const ParametrosLote*)parametros;

@end
