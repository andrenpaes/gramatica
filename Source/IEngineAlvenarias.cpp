//
//  IEngineAlvenarias.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 02/06/13.
//
//

#include "IEngineAlvenarias.h"
#include "Util.h"

Abertura::Abertura(const char* tipo): tipo(copy_cstring(tipo)), retaReferencia(NULL), area(0), comprimento(0), distanciaOrigem(0) {
}

Abertura::~Abertura() {
	if (this->tipo != NULL) {
		free(this->tipo);
	}
	if (this->retaReferencia != NULL) {
		delete this->retaReferencia;
	}
}


Parede::Parede(double xi, double yi, double xf, double yf): reta(Reta(xi, yi, xf, yf)) {
}

Parede::~Parede() {
	while(!aberturas.empty()) {
		delete aberturas.back();
		aberturas.pop_back();
	}
}


Zona::Zona(const char* simbolo, const char* categoria) {
	this->simbolo = copy_cstring(simbolo);
	this->categoria = copy_cstring(categoria);
}

Zona::~Zona() {
	if (this->simbolo != NULL) {
		free(this->simbolo);
	}
	if (this->categoria != NULL) {
		free(this->categoria);
	}
}


IEngineAlvenarias::~IEngineAlvenarias() {
	while(!paredes.empty()) {
		delete paredes.back();
		paredes.pop_back();
	}

	while(!cobertas.empty()) {
		delete cobertas.back();
		cobertas.pop_back();
	}
}
