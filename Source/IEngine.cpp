//
//  IEngine.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 02/06/13.
//
//

#include "IEngine.h"
#include "Util.h"

IEngine::~IEngine() {
	SetErrorDescription(NULL);
	SetLoadSourcePath(NULL);
}

void IEngine::SetErrorDescription(char *errorDescription) {
	if (this->errorDescription) {
		free(this->errorDescription);
	}
	
	this->errorDescription = errorDescription;
}

void IEngine::SetLoadSourcePath(const char *loadSourcePath) {
	if (this->loadSourcePath) {
		free(this->loadSourcePath);
	}
	if (loadSourcePath) {
		this->loadSourcePath = copy_cstring(loadSourcePath);
	} else {
		this->loadSourcePath = NULL;
	}
}

