/**
 * @file Contains the includes and definitions necessary for the Add-On to
 *       function.
 */

#ifndef __Gramatica_H__
#define __Gramatica_H__

#ifdef _WIN32
	#pragma warning (push, 3)
	#include	"Win32Interface.hpp"
	#pragma warning (pop)

	#ifndef WINDOWS
		#define WINDOWS
	#endif
#endif

#ifdef macintosh
	#include	<MacTypes.h>
#endif

#include	<stdio.h>
#include	<stdlib.h>

#ifdef WINDOWS
	#pragma warning (disable: 4068 4786)
#endif

#ifndef ACExtension
	#define	ACExtension
#endif

#include	"ACAPinc.h"

#ifdef WINDOWS
	#pragma warning (default: 4068)
#endif

#endif // __Gramatica_H__
