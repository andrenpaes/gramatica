//
//  GramaticaEditorController.m
//  Gramatica
//
//  Created by Andre Rodrigues on 20/10/12.
//
//

#import "GramaticaDialogController.h"

@interface GramaticaDialogController ()

@end

@implementation GramaticaDialogController

@synthesize zoneamentos;

@synthesize zoneamento;
@synthesize testada;
@synthesize comprimento;
@synthesize azimute;

@synthesize area;

@synthesize returnAction;

@synthesize mensagens;
@synthesize loteView;

+ (GramaticaAction)openDialog:(ParametrosLote *)parametrosLote;
{
	GramaticaDialogController *controller = [[[GramaticaDialogController alloc] init:parametrosLote] autorelease];
	[NSApp runModalForWindow:controller.window];

	return controller.returnAction;
}

- (id)init:(ParametrosLote*)parametros
{
    self = [super initWithWindowNibName:@"GramaticaDialog"];
	if (self) {
		parametrosLote = parametros;
		zoneamentos = [[Zoneamento loadZoneamentos] retain];
	}
    return self;
}

- (void)windowDidLoad
{
	[self.loteView setParametrosLote:parametrosLote];
	self.zoneamento = nil;
	self.testada = parametrosLote->GetTestada();
	self.comprimento = parametrosLote->GetComprimento();
	self.azimute = parametrosLote->GetAzimute();

	self.area = parametrosLote->Area();

	[self addObserver:self forKeyPath:@"zoneamento" options:0 context:nil];
	[self addObserver:self forKeyPath:@"testada" options:0 context:nil];
	[self addObserver:self forKeyPath:@"comprimento" options:0 context:nil];
	[self addObserver:self forKeyPath:@"azimute" options:0 context:nil];
}

- (void)observeValueForKeyPath:(NSString *)__unused keyPath ofObject:(id)__unused object change:(NSDictionary *)__unused change context:(void *)__unused context
{
	[loteView setZoneamento:zoneamento];
	parametrosLote->SetTestada(testada);
	parametrosLote->SetComprimento(comprimento);
	parametrosLote->SetAzimute(azimute);

	self.area = parametrosLote->Area();
	[loteView setNeedsDisplay:YES];
}

- (IBAction)validarParametros:(id)__unused sender
{
	NSArray *erros = [zoneamento validarParametros:parametrosLote];
	if ([erros count] > 0) {
		NSMutableString *mensagem = nil;
		for (NSString *erro in erros) {
			if (mensagem) {
				[mensagem appendString:[NSString stringWithFormat:@"\n%@", erro]];
			} else {
				mensagem = [NSMutableString stringWithString:erro];
			}
		}
		[self.mensagens setStringValue:mensagem];
		NSRunAlertPanel(@"Erro!", @"Foi encontrado um problem nos dado informadas. Favor verificar as mensagens.", @"OK", nil, nil);
	} else {
		returnAction = ActionOk;
		[self.window close];
	}
}

- (IBAction)cancelar:(id)__unused sender
{
	[self.mensagens setStringValue:@"Cancelar"];
	returnAction = ActionCancelar;
	[self.window close];
}

- (void)windowWillClose:(NSNotification *)__unused notification
{
	[NSApp abortModal];
}

- (void)dealloc
{
	[zoneamentos release];
	[zoneamento release];

    [super dealloc];
}

@end
