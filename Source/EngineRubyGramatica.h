//
//  EngineRubyGramatica.h
//  Gramatica
//
//  Created by Andre Rodrigues on 12/01/13.
//
//

#ifndef __Gramatica__EngineRubyGramatica__
#define __Gramatica__EngineRubyGramatica__

#include "IEngineGramatica.h"
#include "EngineRuby.h"

#include <map>

class EngineRubyGramatica: public IEngineGramatica, public EngineRuby {
protected:
	IEngine* GetIEngineInstance();
public:
	EngineRubyGramatica();
	~EngineRubyGramatica();

	EngineError LoadEngine();
	EngineError Process(const ParametrosLote *parametros);
	void ProcessResult(mrb_value resultado);

	const char* GetType() const {
		return "rb";
	}
};

extern std::map<mrb_state*, EngineRubyGramatica*> _engineMap;

#endif /* defined(__Gramatica__EngineRubyGramatica__) */
