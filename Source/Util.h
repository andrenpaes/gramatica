//
//  Util.h
//  Gramatica
//
//  Created by Andre Rodrigues on 07/03/13.
//
//

#ifndef Gramatica_Util_h
#define Gramatica_Util_h

char* copy_cstring(const char* cstring, ...);
char* append_to_path(const char* path, const char* fileName);
char* read_file(const char* filePath);

#endif
