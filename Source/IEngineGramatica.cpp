//
//  IEngineGramatica.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 07/03/13.
//
//

#include "IEngineGramatica.h"

#include "Util.h"

Elemento::Elemento(const char* simbolo, const char* categoria, double x, double y): x(x), y(y) {
	this->simbolo = copy_cstring(simbolo);
	this->categoria = copy_cstring(categoria);
}

Elemento::~Elemento() {
	free(this->simbolo);
	free(this->categoria);
}

IEngineGramatica::~IEngineGramatica() {
	SetResultado(NULL);
}
