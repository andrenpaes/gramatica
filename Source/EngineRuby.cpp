//
//  EngineRuby.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 28/11/13.
//
//

#include "EngineRuby.h"
#include "Util.h"

#include "mruby/compile.h"
#include "mruby/string.h"

EngineRuby::EngineRuby() {
	mrb = mrb_open();
}

EngineRuby::~EngineRuby() {
	mrb_close(mrb);
}

EngineError EngineRuby::ExecuteRubyFile(const char *file) {
	EngineError error = 0;
	
	char* file_path = append_to_path(GetIEngineInstance()->GetLoadSourcePath(), file);
	FILE* rubyFile = fopen(file_path, "r");
	mrb_load_file(mrb, rubyFile);
	if (mrb->exc) {
		mrb_value exception = mrb_obj_value(mrb->exc);
		mrb_value message = mrb_funcall(mrb, exception, "message", 0);
		GetIEngineInstance()->SetErrorDescription(copy_cstring("Erro ao carregar arquivo '%s': %s", file,
															   mrb_string_value_cstr(mrb, &message)));

        mrb_value backtrace = mrb_funcall(mrb, exception, "backtrace", 0);
		mrb_value inspect = mrb_funcall(mrb, backtrace, "inspect", 0);
		puts(mrb_string_value_cstr(mrb, &inspect));
        error = 1;
	}
	fclose(rubyFile);
	free(file_path);
	
	return error;
}

EngineError EngineRuby::HandleError(int lineNumber) {
	if (mrb->exc) {
		mrb_value message = mrb_funcall(mrb, mrb_obj_value(mrb->exc), "message", 0);
		GetIEngineInstance()->SetErrorDescription(copy_cstring("Erro (%s:%i:): %s", __FILE__, lineNumber ,
															   mrb_string_value_cstr(mrb, &message)));
		return 1;
	}
	return 0;
}
