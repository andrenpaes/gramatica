//
//  GramaticaEditorController.h
//  Gramatica
//
//  Created by Andre Rodrigues on 20/10/12.
//
//

#import <Cocoa/Cocoa.h>

#include "ParametrosLote.h"

#import "Zoneamento.h"
#import "LoteView.h"

enum {
    ActionCancelar = 0,
	ActionOk
};
typedef NSUInteger GramaticaAction;

@interface GramaticaDialogController : NSWindowController<NSWindowDelegate>
{
@private
	NSArray* zoneamentos;
	ParametrosLote* parametrosLote;

	Zoneamento* zoneamento;
	double comprimento;
	double testada;
	double azimute;
	double area;

	GramaticaAction returnAction;

	NSTextField* mensagens;
	LoteView* loteView;
}

@property (nonatomic, readonly) NSArray* zoneamentos;

@property (nonatomic, retain) Zoneamento* zoneamento;
@property (nonatomic) double testada;
@property (nonatomic) double comprimento;
@property (nonatomic) double azimute;

@property (nonatomic) double area;

@property (nonatomic, assign) IBOutlet NSTextField* mensagens;
@property (nonatomic, assign) IBOutlet LoteView* loteView;

@property (nonatomic, readonly) GramaticaAction returnAction;

+ (GramaticaAction)openDialog:(ParametrosLote *)parametrosLote;

- (IBAction)validarParametros:(id)sender;
- (IBAction)cancelar:(id)sender;

@end
