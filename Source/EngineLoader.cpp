
#include "EngineLoader.h"

#include "CoreFoundation/CoreFoundation.h"

#include "Util.h"

CFBundleRef getPluginBundle() {
	return CFBundleGetBundleWithIdentifier(CFSTR("com.graphisoft.Gramatica"));
}

char* copyResourcesPath() {
	CFBundleRef pluginBundle = getPluginBundle();

	CFURLRef bundleURL = CFBundleCopyBundleURL(pluginBundle);
	CFStringRef bundlePath = CFURLCopyFileSystemPath(bundleURL, kCFURLPOSIXPathStyle);
	CFIndex bundlePathLength = CFStringGetMaximumSizeForEncoding(CFStringGetLength(bundlePath),
																 kCFStringEncodingUTF8);
	char* bundlePathCString = (char*) calloc(sizeof(char), bundlePathLength);
	CFStringGetCString(bundlePath, bundlePathCString, bundlePathLength, kCFStringEncodingUTF8);
	CFRelease(bundleURL);
	CFRelease(bundlePath);

	CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(pluginBundle);
	CFStringRef resourcesPath = CFURLCopyFileSystemPath(resourcesURL, kCFURLPOSIXPathStyle);
	CFIndex resourcesPathLength = CFStringGetMaximumSizeForEncoding(CFStringGetLength(resourcesPath),
																	kCFStringEncodingUTF8);
	char* resourcesPathCString = (char*) calloc(sizeof(char), resourcesPathLength);
	CFStringGetCString(resourcesPath, resourcesPathCString, resourcesPathLength, kCFStringEncodingUTF8);
	CFRelease(resourcesURL);
	CFRelease(resourcesPath);

	char* resourcesAbsolutePath = append_to_path(bundlePathCString, resourcesPathCString);
	free(bundlePathCString);
	free(resourcesPathCString);
	
	return resourcesAbsolutePath;
}

EngineError Inicializar(IEngine *engine) {
	char* sourceLoadPath = copyResourcesPath();
	engine->SetLoadSourcePath(sourceLoadPath);

	EngineError engineError = engine->LoadEngine();
	if (engineError) {
		return engineError;
	}

	return 0;
}
