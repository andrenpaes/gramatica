//
//  EngineRubyAlvenarias.cpp
//  Gramatica
//
//  Created by Andre Rodrigues on 08/06/13.
//
//

#include "EngineRubyAlvenarias.h"

#include "Util.h"

#include "mruby/array.h"
#include "mruby/compile.h"
#include "mruby/string.h"

IEngineAlvenarias* CreateAlvenariasEngine() {
	return new EngineRubyAlvenarias();
}

IEngine* EngineRubyAlvenarias::GetIEngineInstance() {
	return this;
}

EngineError EngineRubyAlvenarias::LoadEngine() {
	static const int NUMBER_OF_FILES = 6;
	static const char* FILES[NUMBER_OF_FILES] = {"additions.rb", "dynamic_attributes.rb",
		"geometria.rb", "attributes.rb", "engine_alvenarias.rb", "aberturas.rb"};

	EngineError error = 0;
	for (int i = 0; i < NUMBER_OF_FILES; i++) {
		if ((error = ExecuteRubyFile(FILES[i]))) {
			return error;
		}
	}
	stateInstance = mrb_obj_value(mrb_class_get(mrb, "State"));

	return error;
}

EngineError EngineRubyAlvenarias::LoadInfoZona(Zona& zona) {
	std::vector<Coordenada>::iterator coordenadas;
	for (coordenadas = zona.GetCoordenadas().begin(); coordenadas < zona.GetCoordenadas().end(); coordenadas++) {
		Coordenada coordenada = *coordenadas;

		mrb_funcall(mrb, stateInstance, "criar_ponto", 2,
					mrb_float_value(mrb, coordenada.GetX()),
					mrb_float_value(mrb, coordenada.GetY()));
		if (HANDLE_ERROR()) {
			return 1;
		}
	}

	mrb_funcall(mrb, stateInstance, "criar_zona", 2,
				mrb_str_new_cstr(mrb, zona.GetSimbolo()),
				mrb_str_new_cstr(mrb, zona.GetCategoria()));
	if (HANDLE_ERROR()) {
		return 1;
	}

	return 0;
}

EngineError EngineRubyAlvenarias::Process() {
	if (EngineError error = GerarParedes()) {
		return error;
	}
	if (EngineError error = GerarCobertas()) {
		return error;
	}

	return 0;
}

EngineError EngineRubyAlvenarias::GerarParedes() {
	mrb_value paredes = mrb_funcall(mrb, stateInstance, "gerar_paredes", 0);
	if (HANDLE_ERROR()) {
		return 1;
	}

	mrb_int numero_paredes = mrb_ary_len(mrb, paredes);
	for (mrb_int indice_parede = 0; indice_parede < numero_paredes; ++indice_parede) {
		mrb_value parede = mrb_ary_entry(paredes, indice_parede);

		Parede *objetoParede; {
			mrb_value aresta = mrb_funcall(mrb, parede, "aresta", 0);
			mrb_value inicio = mrb_funcall(mrb, aresta, "inicio", 0);
			double xi =  mrb_float(mrb_funcall(mrb, inicio, "x", 0)), yi = mrb_float(mrb_funcall(mrb, inicio, "y", 0));
			mrb_value fim = mrb_funcall(mrb, aresta, "fim", 0);
			double xf =  mrb_float(mrb_funcall(mrb, fim, "x", 0)), yf = mrb_float(mrb_funcall(mrb, fim, "y", 0));
			objetoParede = new Parede(xi, yi, xf, yf);
		}
		objetoParede->SetAltura(mrb_float(mrb_funcall(mrb, parede, "altura", 0)));
		objetoParede->SetEspessura(mrb_float(mrb_funcall(mrb, parede, "espessura", 0)));

		mrb_value aberturas = mrb_funcall(mrb, parede, "aberturas", 0);
		mrb_int numero_aberturas = mrb_ary_len(mrb, aberturas);
		for (mrb_int indice_abertura = 0; indice_abertura < numero_aberturas; ++indice_abertura) {
			mrb_value abertura = mrb_ary_entry(aberturas, indice_abertura);

			Abertura* objetoAbertura; {
				mrb_value tipo_abertura = mrb_funcall(mrb, abertura, "tipo", 0);
				tipo_abertura = mrb_funcall(mrb, tipo_abertura, "to_s", 0);
				objetoAbertura = new Abertura(mrb_string_value_cstr(mrb, &tipo_abertura));
			}

			mrb_value segmento_aresta = mrb_funcall(mrb, abertura, "segmento_aresta", 0);
			mrb_value inicio = mrb_funcall(mrb, segmento_aresta, "inicio", 0);
			double xi =  mrb_float(mrb_funcall(mrb, inicio, "x", 0)), yi = mrb_float(mrb_funcall(mrb, inicio, "y", 0));
			mrb_value fim = mrb_funcall(mrb, segmento_aresta, "fim", 0);
			double xf =  mrb_float(mrb_funcall(mrb, fim, "x", 0)), yf = mrb_float(mrb_funcall(mrb, fim, "y", 0));
			objetoAbertura->SetRetaReferencia(new Reta(xi, yi, xf, yf));

			objetoAbertura->SetArea(mrb_float(mrb_funcall(mrb, abertura, "area", 0)));
			objetoAbertura->SetPeitoril(mrb_float(mrb_funcall(mrb, abertura, "peitoril", 0)));
			objetoAbertura->SetComprimento(mrb_float(mrb_funcall(mrb, abertura, "comprimento", 0)));
			objetoAbertura->SetAltura(mrb_float(mrb_funcall(mrb, abertura, "altura", 0)));
			objetoAbertura->SetDistanciaOrigem(mrb_float(mrb_funcall(mrb, abertura, "distancia_origem", 0)));

			objetoParede->GetAberturas().push_back(objetoAbertura);
		}

		this->paredes.push_back(objetoParede);
	}
	return 0;
}

EngineError EngineRubyAlvenarias::GerarCobertas() {
	mrb_value cobertas = mrb_funcall(mrb, stateInstance, "gerar_cobertas", 0);
	if (HANDLE_ERROR()) {
		return 1;
	}

	mrb_int numero_cobertas = mrb_ary_len(mrb, cobertas);
	for (mrb_int indice_coberta = 0; indice_coberta < numero_cobertas; ++indice_coberta) {
		mrb_value coberta = mrb_ary_entry(cobertas, indice_coberta);
		Coberta* objetoCoberta; {
			mrb_value baseline = mrb_funcall(mrb, coberta, "baseline", 0);

			mrb_int sinal_pivo = mrb_fixnum(mrb_funcall(mrb, coberta, "sinal_pivo", 0));

			mrb_value inicio = mrb_funcall(mrb, baseline, "inicio", 0);
			double xi =  mrb_float(mrb_funcall(mrb, inicio, "x", 0)), yi = mrb_float(mrb_funcall(mrb, inicio, "y", 0));
			mrb_value fim = mrb_funcall(mrb, baseline, "fim", 0);
			double xf =  mrb_float(mrb_funcall(mrb, fim, "x", 0)), yf = mrb_float(mrb_funcall(mrb, fim, "y", 0));

			objetoCoberta = new Coberta(Reta(xi, yi, xf, yf), sinal_pivo);
		}
		objetoCoberta->SetAltura(mrb_float(mrb_funcall(mrb, coberta, "altura", 0)));
		objetoCoberta->SetEspessura(mrb_float(mrb_funcall(mrb, coberta, "espessura", 0)));
		objetoCoberta->SetAnguloInclinacao(mrb_float(mrb_funcall(mrb, coberta, "angulo_inclinacao", 0)));

		mrb_value pontos = mrb_funcall(mrb, coberta, "pontos", 0);
		mrb_int numero_pontos = mrb_ary_len(mrb, pontos);
		for (mrb_int indice_ponto = 0; indice_ponto < numero_pontos; indice_ponto++) {
			mrb_value ponto = mrb_ary_entry(pontos, indice_ponto);
			double x =  mrb_float(mrb_funcall(mrb, ponto, "x", 0)), y = mrb_float(mrb_funcall(mrb, ponto, "y", 0));
			objetoCoberta->GetPontos().push_back(Coordenada(x, y));
		}

		this->cobertas.push_back(objetoCoberta);
	}
	return 0;
}
