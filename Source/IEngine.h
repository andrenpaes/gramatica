//
//  IEngine.h
//  Gramatica
//
//  Created by Andre Rodrigues on 02/06/13.
//
//

#ifndef __Gramatica__IEngine__
#define __Gramatica__IEngine__

#include <stdlib.h>

typedef int EngineError;

struct IEngine {
protected:
	char *loadSourcePath;
	char *errorDescription;
	
	IEngine(): loadSourcePath(NULL), errorDescription(NULL) {};
public:
	virtual ~IEngine();

	virtual EngineError LoadEngine() = 0;

	void SetLoadSourcePath(const char* loadSourcePath);
	const char* GetLoadSourcePath() const {
		return this->loadSourcePath;
	}

	void SetErrorDescription(char *errorDescription);
	const char* GetErrorDescription() const {
		return this->errorDescription;
	}
};

#endif /* defined(__Gramatica__IEngine__) */
